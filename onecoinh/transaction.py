""" Classes for transaction """
import json
from typing import Union

import qrcode

from .constants import TransactionType
from .shared import generate_hash


class Transaction:  # pylint: disable=too-few-public-methods,too-many-instance-attributes
    """
    Represents a Transaction.

    If a transaction is created with sender set to None,
    an initial deposit to set the balance.
    """

    def __init__(
            self,
            sender: Union[str, None],
            receiver: str,
            transaction_type: TransactionType,
            amount: float,
            description: str
    ):  # pylint: disable=too-many-arguments
        self.sender: Union[str, None] = sender
        self.receiver: str = receiver
        self.transaction_type = transaction_type  # Type of the Transaction.
        self.description: str = description
        self.amount: float = amount
        self.hash = generate_hash(self.json())

    def _encode(self) -> bytes:
        """ Encode the json representation into binary string """
        _bytes = json.dumps(self.json()).encode('utf-8')
        return _bytes

    def generate_code(self):
        """ Generate a code """
        return qrcode.make(self._encode())

    def json(self):
        """ Generate compressed json for the transaction """
        data = {
            "s": self.sender,  # sender
            "r": self.receiver,  # receiver
            "t": self.transaction_type.value,  # type
            "x": self.description,
            "a": self.amount
        }
        return data
