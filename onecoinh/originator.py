""" Classes for Originator """
import json
import qrcode


class Originator:  # pylint: disable=too-few-public-methods,too-many-instance-attributes
    """
    Represents an Originator.
    """

    def __init__(
            self,
            identity: int,  # the index pointing to an Identity block on the chain
            amount: float
    ):  # pylint: disable=too-many-arguments
        self.identity: int = identity
        self.amount: float = amount

    def _encode(self) -> bytes:
        """ Encode the json representation into binary string """
        _bytes = json.dumps(self.json()).encode('utf-8')
        return _bytes

    def generate_code(self):
        """ Generate a code """
        return qrcode.make(self._encode())

    def json(self):
        """ Generate compressed json for the transaction """
        data = {
            "i": self.identity,
            "a": self.amount
        }
        return data
