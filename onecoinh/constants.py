""" OneCoinH Constants """
from enum import Enum
from typing import Tuple


CURRENCY_SYMBOL = "ᕫ"
MICRO_SYMBOL = "\u00B5" + CURRENCY_SYMBOL


class TransactionType(str, Enum):
    """ Possible types """
    DEBIT = 'debit'  # Deduction
    CREDIT = 'credit'  # Addition

    @staticmethod
    def from_str(label):
        """ Get the enum from the label """
        if label == 'debit':
            return TransactionType.DEBIT
        if label == 'credit':
            return TransactionType.CREDIT
        raise NotImplementedError


class TerminalHelper:  # pylint: disable=too-few-public-methods
    """ Helper class for terminal colors """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class ColorSet:  # pylint: disable=too-few-public-methods
    """ Individual Color Line for Colors"""
    name: str
    rgb: Tuple[int, int, int]

    def __init__(self, iterable=(), **kwargs):
        """ Do some magic to create the Class Instance from dict """
        self.__dict__.update(iterable, **kwargs)


class Colors(Enum):  # pylint: disable=too-few-public-methods
    """ Colors used by onecoinh with various functions attached
    Colors used from https://www.colorhexa.com/color-names
    """
    RED = ColorSet(
        {"name": "red", "rgb": (229, 43, 80)}  # Amaranth
    )
    PINK = ColorSet(
        {"name": "pink", "rgb": (255, 192, 203)}
    )
    PURPLE = ColorSet(
        {"name": "purple", "rgb": (128, 0, 128)}
    )
    DEEPPURPLE = ColorSet(
        {"name": "deeppurple", "rgb": (105, 53, 156)}  # Purple Heart
    )
    INDIGO = ColorSet(
        {"name": "indigo", "rgb": (75, 0, 130)}
    )
    BLUE = ColorSet(
        {"name": "blue", "rgb": (0, 127, 255)}  # Azure
    )
    LIGHTBLUE = ColorSet(
        {"name": "lightblue", "rgb": (161, 202, 241)}  # Baby blue eyes
    )
    CYAN = ColorSet(
        {"name": "cyan", "rgb": (0, 255, 255)}
    )
    TEAL = ColorSet(
        {"name": "teal", "rgb": (0, 128, 128)}
    )
    GREEN = ColorSet(
        {"name": "green", "rgb": (42, 128, 0)}  # Napier Green
    )
    LIGHTGREEN = ColorSet(
        {"name": "lightgreen", "rgb": (201, 220, 135)}  # Medium spring bud
    )
    LIME = ColorSet(
        {"name": "lime", "rgb": (192, 255, 0)}
    )
    YELLOW = ColorSet(
        {"name": "yellow", "rgb": (255, 244, 79)}  # Lemon Yellow
    )
    AMBER = ColorSet(
        {"name": "amber", "rgb": (255, 191, 0)}
    )
    ORANGE = ColorSet(
        {"name": "orange", "rgb": (255, 165, 0)}
    )
    DEEPORANGE = ColorSet(
        {"name": "deeporange", "rgb": (255, 69, 0)}  # Orange red
    )
    BROWN = ColorSet(
        {"name": "brown", "rgb": (165, 42, 42)}
    )
    GRAY = ColorSet(
        {"name": "gray", "rgb": (128, 128, 128)}
    )
    BLUEGRAY = ColorSet(
        {"name": "bluegray", "rgb": (83, 104, 149)}  # UCLA Blue
    )

    def hex_color(self):
        """ Create a hex code for the code to be used on the web """
        return '#' + ''.join(f'{i:02X}'.lower() for i in self.value.rgb)

    def kivy_color(self):
        """ Export the color Tuple required by Kivy
        which is a rgba where values are between 0 and 1 and a
        fourth indicator is added to indicate opacity """
        _c = self.value.rgb
        return _c[0]/255, _c[1]/255, _c[2]/255, 1


def get_color_list():
    """ Get a list of the color names as str """
    return [el.name.lower() for el in Colors]
