""" Shared Module """
import json
from hashlib import sha256
from typing import Union


def generate_hash(representation: dict[str, Union[str, int]]):
    """ Generate a hash for identity or transaction """
    return sha256(
        json.dumps(
            representation
        ).encode('utf-8')
    ).hexdigest()
