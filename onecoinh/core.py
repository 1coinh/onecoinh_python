""" OneCoinH core module """
import json
import pickle
from datetime import datetime
from pathlib import Path
from typing import List, Union
import webbrowser

from PIL import Image
from pyzbar.pyzbar import decode

from .identity import Identity
from .transaction import Transaction, TransactionType
from .blockchain import Blockchain


def decode_transaction_data(encoded: bytes):
    """ Decode the binary string representation into json """
    _json = json.loads(encoded.decode('utf-8'))
    not_set = []
    if "s" not in _json:
        not_set.append("sender")
    if "r" not in _json:
        not_set.append("receiver")
    if "t" not in _json:
        not_set.append("transaction_type")
    if "a" not in _json:
        not_set.append("amount")
    if "x" not in _json:
        not_set.append("description")
    if len(not_set) > 0:
        raise ValueError(f"{', '.join(not_set)} are missing from the transaction")
    return Transaction(
        _json["s"],
        _json["r"],
        TransactionType.from_str(_json["t"]),
        _json["a"],
        _json["x"]
    )


def decode_identity_data(encoded: bytes):
    """ Decode the binary string representation into json """
    _name = None
    _dob = None
    _height = None
    _weight = None
    _gender = "other"
    _hair = None
    _eyes = (None, None)
    _features = None

    _json = json.loads(encoded.decode('utf-8'))
    if "n" in _json:
        _name = _json["n"]
    if "d" in _json:
        _dob = datetime.strptime(_json["d"], "%Y-%m-%d")
    if "h" in _json:
        _height = _json["h"]
    if "w" in _json:
        _weight = _json["w"]
    if "g" in _json:
        if _json["g"] == "f":
            _gender = "female"
        if _json["g"] == "m":
            _gender = "male"

    if "z" in _json:
        _hair = _json["z"]

    if "l" in _json:
        _l = list(_eyes)
        _l[0] = _json["l"]
        _eyes = tuple(_l)
    if "r" in _json:
        _l = list(_eyes)
        _l[1] = _json["r"]
        _eyes = tuple(_l)
    if "x" in _json:
        _features = _json["x"]

    return Identity(
        _name,
        _dob,
        _height,
        _weight,
        _hair,
        _eyes,
        _gender,
        _features
    )


def decode_qr(
        filename: Union[str, Path]
) -> Union[Identity, Transaction, None]:
    """ Try to decode a QR Image """
    img = Image.open(filename)
    decoded_list = decode(img)
    # check that hashing the identity results in the hash that is in the filename
    assert len(decoded_list) > 0
    _data: bytes = decoded_list[0].data
    _json: dict = json.loads(_data.decode('utf-8'))
    if "n" in _json.keys():
        _identity = decode_identity_data(encoded=_data)
        return _identity
    if "a" in _json.keys():
        _transaction = decode_transaction_data(encoded=_data)
        return _transaction
    return None


class OneCoinH:
    """
    OneCoinH class

    Main class that holds all identities, the personal blockchain and the personal ledger
    """
    epoch = datetime(2022, 1, 1, 0)
    identity_pkl = Path('data/identity')
    identity: Identity = None
    blockchain: Blockchain = None
    _previous_identities: List[Identity] = []
    generated: float = None
    total_depreciated_self_created_coins: float = None

    def __init__(self):
        """ Initialise with and try to restore information when this is not the first run """
        if self.identity_pkl.is_file():
            with open(self.identity_pkl, 'rb') as f:
                self.identity = pickle.load(f)
        if self.identity is not None:
            self.identity.save()
            self._initialize()
            self.blockchain = Blockchain(self.identity.hash)
        else:
            self.identity = Identity()

    def _initialize(self):
        """
        Given that one coin per hour is created from date of birth
        till now generate the total depreciated self created coins
        """
        self.start_date = self._get_start_date()
        self.generated = self._coins_pre_devaluation()
        self.total_depreciated_self_created_coins = self._get_total_depreciated_self_created_coins()

    def _get_deprecation(self):
        """
        All self created coins are being devaluated with a factor of 0.99995 per
        hour. As a result of this devaluation, de maximum coins per person that
        is participating, can not surpass 20,000 coins. This is because when a
        person gets close to 20,000 coins, in the hour to add a new self created
        coin, the previous created coins devaluate with:

        19,999.9999 x 0.00005 = 0.999999995 coins.

        It will take a person about 41 years to accumulate 19,999.9999 coins but
        will never reach 20,000. This is how the money supply is held constant
        and inflation through printing money is eliminated completely.
        """
        return 0.99995

    def _get_start_date(self):
        """ Get the start date for the user """
        _start_date = datetime.combine(self.identity.dob, datetime.min.time())
        return max(_start_date, self.epoch)

    def _coins_pre_devaluation(self):
        """
        The hours till now() is the equivalent of the
        number of coins a user would have if no devaluation took
        place.
        """
        _difference = datetime.now().replace(microsecond=0, second=0, minute=0) - self.start_date
        _hours = divmod(_difference.total_seconds(), 3600)
        return _hours[0]

    def _get_total_depreciated_self_created_coins(self) -> float:
        """ Calculate the total depreciated self created coins """
        self.generated = self._coins_pre_devaluation()
        _balance = 0.0
        for _ in range(int(self.generated)):
            _balance = (_balance*0.99995) + 1.0
        return _balance

    def birthday_epoch_diff(self):
        """ Get the difference in hours between the epoch and the date of birth """
        if self.identity is None:
            raise ValueError("Identity not set")
        if self.identity.dob is None:
            raise ValueError("Identity Date of Birth not set")

        _start_date = datetime.combine(self.identity.dob, datetime.min.time())
        return divmod((_start_date - self.epoch).total_seconds(), 3600)[0]

    def print_transactions_per_user(self):
        """ Print a transaction log per user """
        self._initialize()
        self.blockchain.print_transactions_per_user()

    def print_transactions(self):
        """ Print a transaction log """
        self._initialize()
        self.blockchain.print_transactions(
            self.total_depreciated_self_created_coins,
            self.generated
        )

    # pylint: disable-next=too-many-arguments
    def send_transaction(
            self,
            receiver: str,
            amount: float,
            description: str,
            validated: bool = False,

    ) -> Transaction:
        """ Log a transaction and subtract from balance """
        _transaction = Transaction(
                self.identity.hash,
                receiver,
                TransactionType.DEBIT,
                amount,
                description
            )
        self.blockchain.add_new(
            _transaction
        )

        if validated:
            self.blockchain.mine()
        return _transaction

    # pylint: disable-next=too-many-arguments
    def receive_transaction(
            self,
            sender: Union[str, None],
            amount: float,
            description: str,
            validated: bool = False
    ):
        """ Log a transaction and add from balance """
        self.blockchain.add_new(
            Transaction(
                sender,
                self.identity.hash,
                TransactionType.CREDIT,
                amount,
                description
            )
        )
        if validated:
            self.blockchain.mine()

    def balance(self) -> float:
        """ Get the balance from the blockchain """
        return self.blockchain.balance(self.total_depreciated_self_created_coins)

    def set_identity(self, identity: Identity):
        """ Set the identity and initialize """
        if self.identity is not None and self.identity.is_complete():
            # todo, hash and push previous identity into _previous_identities
            raise ValueError("Identity has already been created")

        self.identity = identity
        with open(self.identity_pkl, 'wb') as f:
            pickle.dump(self.identity, f)
        self.blockchain = Blockchain(self.identity.hash)
        self._initialize()

    def validate_transactions(self):
        """
        Transactions in general are not stored directly, and can be
        accepted in batch using this function
        """
        self.blockchain.mine()

    def identity_history(self):
        """ Get hashes for the previous identities"""
        return self._previous_identities

    def has_previous_identities(self) -> bool:
        """ Check to see if we have previous identities, returns True or False """
        if len(self.identity_history()) > 0:
            return True
        return False


def show_manual():
    """ Show the manual text """
    webbrowser.open_new('./assets/Manual_2_01_01.pdf')


def show_disclaimer():
    """ Show the manual text """
    webbrowser.open_new('./assets/Disclaimer_01_04_2024.pdf')
