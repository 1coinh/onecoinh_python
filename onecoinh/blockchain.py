""" Blockchain module """
import json
import pickle
import time
from datetime import date, datetime
from hashlib import sha256
from pathlib import Path
from typing import List, TypeVar, Generic, Union

from .transaction import Transaction
from .identity import Identity
from .constants import TerminalHelper, TransactionType

T = TypeVar("T")


def serialize(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, date):
        serial = obj.isoformat()
        return serial

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial

    return obj.__dict__


class Block(Generic[T]):
    """ Block class """

    def __init__(
            self,
            index: int,
            data: T,
            timestamp,
            previous_hash: str,
            nonce: int = 0
    ):  # pylint: disable=too-many-arguments
        self.index = index
        self.data: T = data
        self.timestamp = timestamp
        self.previous_hash = previous_hash
        self.nonce = nonce
        self.hash = self.compute_hash()

    def compute_hash(self) -> str:
        """ Compute the hash for a block """
        return sha256(self.dump().encode()).hexdigest()

    def dump(self) -> str:
        """ Dump the block as a json string """
        return json.dumps(self.__dict__, default=serialize, sort_keys=True)


class IdentityBlock(Block[Identity]):
    """ Identity Block """


class TransactionBlock(Block[List[Transaction]]):
    """ Transaction Block"""


class Blockchain:
    """ Blockchain class """
    difficulty = 2
    blockchain_pkl = None
    pending_parts_pkl = None
    pending_parts: List[Union[Transaction, Identity]] = None
    chain: List[Block] = None

    def __init__(self, identity_hash: str):
        self.pending_parts = []
        self.chain = []

        if identity_hash is not None:
            self.blockchain_pkl = Path(f'data/{identity_hash}.chain')
            self.pending_parts_pkl = Path(
                # file to temporarily store the parts of a transaction
                f'data/.{identity_hash}.parts'
            )

            if self.blockchain_pkl.is_file():
                with open(self.blockchain_pkl, 'rb') as f:
                    self.chain = pickle.load(f)
            else:
                if self.pending_parts_pkl.is_file():
                    with open(self.pending_parts_pkl, 'rb') as f:
                        self.pending_parts = pickle.load(f)

            if self.chain == [] and self.pending_parts == []:
                print("New blockchain, creating genesis block")
                self.create_genesis_block()

    def create_genesis_block(self):
        """
        Create the very first block on the chain
        (in blockchain lingo often called the genesis block).
        """
        genesis_block = Block(0, [], time.time(), "0")
        self.append_and_store(genesis_block)

    @property
    def chain_length(self) -> int:
        """ Get the length of the chain """
        return len(self.chain)

    @property
    def last_block(self) -> Block:
        """ Get the last block on the chain """
        return self.chain[-1]

    def proof_of_work(self, block: Block) -> str:
        """ Generate proof of work """
        computed_hash = block.compute_hash()
        while not computed_hash.startswith('0' * self.difficulty):
            block.nonce += 1
            computed_hash = block.compute_hash()
        return computed_hash

    def add_block(self, block: Union[Block, TransactionBlock, IdentityBlock], proof) -> bool:
        """ Add a block to the chain """
        previous_hash = self.last_block.compute_hash()
        if previous_hash != block.previous_hash:
            return False
        if not self.is_valid_proof(block, proof):
            return False
        block.hash = proof
        self.append_and_store(block)
        return True

    def is_valid_proof(self, block, block_hash) -> bool:
        """ Checks if the proof is valid and returns a boolean """
        return block_hash.startswith('0' * self.difficulty) and block_hash == block.compute_hash()

    def add_new(self, data: Union[Transaction, Identity]):
        """ Add a new Transaction or Identity to data """
        self.append_and_store_pending(data)

    def balance(self, _balance: float):
        """ Checks the blockchain and generates the balance """
        for block in self.chain:
            for transaction in block.data:
                if transaction.transaction_type == TransactionType.CREDIT:
                    _balance = _balance + transaction.amount
                else:
                    _balance = _balance - transaction.amount
        return _balance

    def print_transactions(self, opening_balance: float, generated: float):
        """ Print a transaction log """
        _balance = opening_balance
        column_widths = [19, 6, 24, 50, 12, 12]

        print(
            'timestamp'.ljust(column_widths[0]),
            'type'.ljust(column_widths[1]),
            'account'.ljust(column_widths[2]),
            'description'.ljust(column_widths[3]),
            'amount'.ljust(column_widths[4]),
            'balance'.ljust(column_widths[5])
        )

        print(
            '_' * column_widths[0],
            '_' * column_widths[1],
            '_' * column_widths[2],
            '_' * column_widths[3],
            '_' * column_widths[4],
            '_' * column_widths[5]
        )
        print(
            ' ' * column_widths[0],
            ' ' * column_widths[1],
            f"generated          {TerminalHelper.OKGREEN}{generated:.0f}"
            f"{TerminalHelper.ENDC}".ljust(column_widths[2]),
            " " * column_widths[3],
            f"{TerminalHelper.FAIL}     {generated - opening_balance:.2f}"
            f"{TerminalHelper.ENDC}".rjust(column_widths[4]),
            f"{TerminalHelper.OKGREEN}    {opening_balance:.2f}"
            f"{TerminalHelper.ENDC}".rjust(column_widths[5])
        )
        for block in self.chain:
            for item in block.data:
                if isinstance(item, Transaction):
                    if item.transaction_type == TransactionType.CREDIT:
                        _balance = _balance + item.amount
                        _amount = f"+{item.amount:.2f}"
                        if item.sender is None:
                            _user = ""
                        else:
                            _user = item.sender[:8] + '........' + item.sender[56:]
                    else:
                        _balance = _balance - item.amount
                        _amount = f"-{item.amount:.2f}"
                        _user = item.receiver[:8] + '........' + item.receiver[56:]
                    print(
                        datetime.fromtimestamp(block.timestamp).strftime('%Y-%m-%dT%H:%M:%S'),
                        item.transaction_type.value.ljust(column_widths[1]),
                        _user.ljust(column_widths[2]),
                        item.description.ljust(column_widths[3]),
                        _amount.rjust(column_widths[4]),
                        f"{_balance:.2f}".rjust(column_widths[5])
                    )

        if _balance >= 0:
            _balance_statement = f"    {TerminalHelper.OKGREEN}{_balance:.2f} \
            {TerminalHelper.ENDC}".rjust(column_widths[5])
        else:
            _balance_statement = f"    {TerminalHelper.FAIL}{_balance:.2f} \
            {TerminalHelper.ENDC}".rjust(column_widths[5])

        print(
            ' ' * column_widths[0],
            ' ' * column_widths[1],
            ' ' * column_widths[2],
            ' ' * column_widths[3],
            ' ' * column_widths[4],
            _balance_statement
        )

    def print_transactions_per_user(self):
        """ Aggregate all transactions to users """
        column_widths = [24, 12, 12, 12]

        print(
            'HASH'.ljust(column_widths[0]),
            'DEBIT'.rjust(column_widths[1]),
            'CREDIT'.rjust(column_widths[2]),
            'BALANCE'.rjust(column_widths[3])
        )

        print(
            '_' * column_widths[0],
            '_' * column_widths[1],
            '_' * column_widths[2],
            '_' * column_widths[3],
        )
        _users = {}
        _total_transactions = 0
        for block in self.chain:
            for item in block.data:
                if isinstance(item, Transaction):
                    if item.transaction_type == TransactionType.CREDIT:
                        _user = item.sender[:8] + '........' + item.sender[56:]
                        _total_transactions = _total_transactions + item.amount
                        if _user not in _users:
                            _users[_user] = {
                                TransactionType.DEBIT.value: [],
                                TransactionType.CREDIT.value: [item.amount]
                            }
                        else:
                            _users[_user][TransactionType.CREDIT.value].append(item.amount)
                    else:
                        _user = item.receiver[:8] + '........' + item.receiver[56:]
                        _total_transactions = _total_transactions - item.amount
                        if _user not in _users:
                            _users[_user] = {
                                TransactionType.DEBIT.value: [item.amount],
                                TransactionType.CREDIT.value: []
                            }
                        else:
                            _users[_user][TransactionType.DEBIT.value].append(item.amount)

        _balance = 0
        for _key, _u in _users.items():
            _balance = sum(
                _u[TransactionType.CREDIT.value]
            ) - sum(_u[TransactionType.DEBIT.value])
            print(
                _key.ljust(column_widths[0]),
                f"{(sum(_u[TransactionType.DEBIT.value])):.2f}".rjust(column_widths[1]),
                f"{sum(_u[TransactionType.CREDIT.value]):.2f}".rjust(column_widths[2]),
                f"{_balance:.2f}".rjust(column_widths[3])
            )

        if _balance >= 0:
            _balance_statement = f"    {TerminalHelper.OKGREEN}{_total_transactions:.2f} \
                                 {TerminalHelper.ENDC}".rjust(column_widths[3])
        else:
            _balance_statement = f"    {TerminalHelper.FAIL}{_total_transactions:.2f} \
                                 {TerminalHelper.ENDC}".rjust(column_widths[3])

        print(
            ' ' * column_widths[0],
            ' ' * column_widths[1],
            ' ' * column_widths[2],
            _balance_statement
        )

    def append_and_store(self, block: Union[Block, TransactionBlock, IdentityBlock]):
        """ Append a block to the chain and store the chain in a pickle """
        self.chain.append(block)
        with open(self.blockchain_pkl, 'wb') as f:
            pickle.dump(self.chain, f)

    def append_and_store_pending(self, data: Union[Transaction, Identity]):
        """ Append originator """
        self.pending_parts.append(data)
        with open(self.pending_parts_pkl, 'wb') as f:
            pickle.dump(self.pending_parts, f)

    def json(self, indent: int = 0):
        """ Return the chain as json """
        _json = []
        for block in self.chain:
            _json.append(json.loads(block.dump()))
        if indent > 0:
            return json.dumps(_json, indent=indent)
        return json.dumps(_json)

    def mine(self) -> int:
        """ Mine the originators, create block and add the block to the chain """
        if self.pending_parts is None or len(self.pending_parts) == 0:
            if self.pending_parts_pkl.is_file():
                self.pending_parts_pkl.unlink()
            return False
        last_block = self.last_block
        new_block = Block(
            index=last_block.index + 1,
            data=self.pending_parts,
            timestamp=time.time(),
            previous_hash=last_block.compute_hash()
        )
        proof = self.proof_of_work(new_block)
        self.add_block(new_block, proof)
        self.pending_parts: List[Union[Transaction, Identity]] = []
        if self.pending_parts_pkl.is_file():
            self.pending_parts_pkl.unlink()
        return new_block.index
