""" Identity Module """
import json
from datetime import date
from io import BytesIO
from typing import Union, Tuple, Optional

import qrcode

from onecoinh.shared import generate_hash


class Identity:  # pylint: disable=too-many-instance-attributes
    """ Identity class """
    image: bytes = None
    name: str = None
    dob: date = None
    height: Union[float, int] = None
    weight: Union[float, int] = None
    hair: str = None
    eyes: Union[str, Tuple[Union[str, None], Union[str, None]]] = (None, None)
    gender: str = None
    features: str = None
    hash: str = None

    # pylint: disable-next=too-many-arguments
    def __init__(self, name: Optional[str] = None,
                 date_of_birth: Optional[date] = None,
                 height: Optional[Union[float, int]] = None,
                 weight: Optional[Union[float, int]] = None,
                 hair_color: Optional[str] = None, eyes: Optional[
                Union[str, Tuple[Union[str, None], Union[str, None]]]] = (
                    None, None), gender: Optional[str] = None,
                 features: Optional[str] = None, image: Union[
                str, bytes] = None):
        self.name = name
        self.dob = date_of_birth
        if isinstance(eyes, str):
            self.eyes = (eyes, eyes)
        if isinstance(eyes, Tuple):
            self.eyes = (eyes[0], eyes[1])
        self.height = height
        self.weight = weight
        self.hair = hair_color
        self.gender = gender
        self.features = features
        if image:
            try:
                if isinstance(image, str):
                    with open(f"assets/{image}", "rb") as f:
                        self.image = BytesIO(f.read()).getvalue()
                elif isinstance(image, bytes):
                    self.image = image

            except FileNotFoundError as e:
                print("Error", e)
        if self.is_complete():
            self.save()

    def save(self):
        """ Set the hash """
        if not self.is_complete():
            raise ValueError("Identity is incomplete, can't save")
        # create based on this identity
        self.hash = generate_hash(self.json())

    def is_complete(self) -> bool:
        """ Report if the Identity is complete or not """
        return (self.image is not None and self.name is not None
                and self.dob is not None)

    def set_name(self, name: str):
        """ Setter for name """
        self.name = name

    def set_dob(self, dob: date):
        """ Setter for date of birth (dob) """
        self.dob = dob

    def set_image(self, image: bytes):
        """ Setter for the Identity Image """
        self.image = image

    def set_height(self, height: Union[float, int]):
        """ Setter for the height """
        self.height = height

    def set_weight(self, weight: Union[float, int]):
        """ Setter for the weight """
        self.weight = weight

    def generate_card(self):
        """
        Generate a card using all set
        properties and the manipulated image
        """

    def _encode(self) -> bytes:
        """ Encode the json representation into binary string """
        _bytes = json.dumps(self.json()).encode('utf-8')
        return _bytes

    def generate_code(self):
        """ Generate a code """
        return qrcode.make(self._encode())

    def backup(self):
        """ Backup the identity to a file """

    def restore(self):
        """ Restore the identity from a file """

    def json(self):
        """ Generate compressed json for the identity """

        data = {"n": self.name, "d": self.dob.strftime("%Y-%m-%d"),
                # date of birth
                }

        if self.height is not None:
            data["h"] = self.height
        if self.weight is not None:
            data["w"] = self.weight
        if self.gender is not None:
            # Get the first character from gender
            data["g"] = self.gender[0]
        if self.hair is not None:
            data["z"] = self.hair
        if self.eyes[0] is not None:
            data["l"] = self.eyes[0]
        if self.eyes[1] is not None:
            data["r"] = self.eyes[1]
        if self.features is not None:
            data["x"] = self.features.strip()
        return data

    def age(self):
        """ Get the age of the Identity """
        today = date.today()
        years = today.year - self.dob.year
        if today.month < self.dob.month or (
                today.month == self.dob.month and today.day < self.dob.day):
            years -= 1
        return years

    def describe(self):
        """ Return readable text for the class instance """
        return f"{self.name} is {self.age()} years old." \
               f"{self._describe_body()} {self._describe_face()}"

    def _describe_face(self):
        """ Return readable text for the face """
        gender_type = "He/She"
        if self.gender == "male":
            gender_type = "He"
        if self.gender == "female":
            gender_type = "She"

        if self.eyes[0] == self.eyes[1]:
            return f"{gender_type} has {self.hair} hair, {self.eyes[0]} eyes."
        if self.eyes[0] is None and self.eyes[1] is not None:
            return f"{gender_type} has {self.hair} hair, " \
                   f" no left eye and a {self.eyes[1]} right eye."
        if self.eyes[0] is not None and self.eyes[1] is None:
            return f"{gender_type} has {self.hair} hair, " \
                   f" a {self.eyes[0]} left eye and no right eye."
        return f"{gender_type} has {self.hair} hair, " \
               f"{self.eyes[0]} left eye and {self.eyes[1]} right eye."

    def _describe_body(self):
        """ Return readable text for the body """
        if self.height is None and self.weight is None:
            return ""
        if self.height is not None and self.weight is None:
            return f"{self.height:.2f}m tall"
        if self.weight is not None and self.weight is None:
            return f"weighs {self.height:.2f}kg"

        return f"{self.height:.2f}m tall and weighs {self.weight:.2f}kg."
