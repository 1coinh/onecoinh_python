from datetime import datetime, timedelta

color_list = {
    "red": (244 / 255, 67 / 255, 54 / 255, 1),
    "pink": (233 / 255, 30 / 255, 99 / 255, 1),
    "purple": (156 / 255, 39 / 255, 176 / 255, 1),
    "deeppurple": (103 / 255, 58 / 255, 183 / 255, 1),
    "indigo": (63 / 255, 81 / 255, 181 / 255, 1),
    "blue": (33 / 255, 150 / 255, 243 / 255, 1),
    "lightblue": (3 / 255, 169 / 255, 244 / 255, 1),
    "cyan": (0, 188 / 255, 212 / 255, 1),
    "teal": (0, 150 / 255, 136 / 255, 1),
    "green": (76 / 255, 175 / 255, 80 / 255, 1),
    "lightgreen": (139 / 255, 195 / 255, 74 / 255, 1),
    "lime": (205 / 255, 220 / 255, 57 / 255, 1),
    "yellow": (1, 235 / 255, 59 / 255, 1),
    "amber": (1, 193 / 255, 7 / 255, 1),
    "orange": (1, 152 / 255, 0, 1),
    "deeporange": (1, 87 / 255, 34 / 255, 1),
    "brown": (121 / 255, 85 / 255, 72 / 255, 1),
    "gray": (158 / 255, 158 / 255, 158 / 255, 1),
    "bluegray": (96 / 255, 125 / 255, 139 / 255, 1)
}


def get_datetime(transaction_line: str) -> datetime:
    try:
        return datetime.strptime(transaction_line[0:18], '%d-%b-%Y,%H:%Mh')
    except ValueError:
        # 24 is not a valid hour
        _time = transaction_line[0:18].replace("24:00", "23:00")
        sdat = datetime.strptime(_time, '%d-%b-%Y,%H:%Mh')
        sdat += timedelta(hours=1)
        return sdat


def get_transaction_level(value: int) -> int:
    """ get the payer part of the transaction level indicator """
    if 2 < value < 6:
        return 1
    if 5 < value < 10:
        return 2
    if 9 < value < 20:
        return 3
    if 19 < value < 50:
        return 4
    if 49 < value < 100:
        return 5
    if 99 < value < 200:
        return 6
    if 199 < value < 500:
        return 7
    if 499 < value < 1000:
        return 8
    if value > 999:
        return 9
    return 0
