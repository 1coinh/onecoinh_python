=============
FUNCTIONS.TXT
=============

The 1CoinH workflow will remain basically the same. The aim is to simplify the personal blockchains (PB's)
even further to a sort of bare minimum. The concept of choosing max 5 different originators of coins at a 
single transaction will remain to keep the user interface small. In this iteration we will add the cross 
checking of each other's collected PB's so we need several new procedures for that, which includes a 
comprehensive way of PB and IDCard collection, storage and processing.

With the cross checking we will also have some extra processes that are fired when fraud/error is detected.
The main action of the user is to refuse the transaction, but we will also need to think about a sort of 
blacklisting system. This will be personal, because it will be impossible for the software to decide who 
is exactly at fault. Blacklisting basically will tell the user how much of his coins might be corrupted 
and could cause issues when using them. Not more than that. When people want to share their blacklist 
issues, it is basically up to them. We should not try to get ourselves in that snakepit. Our position is 
simply that people should act honest and careful not to receive corrupted coins. We can give them 
directions how to be prudent, but we will leave all resposibility at the users. We will only create a 
system that can detect fraud/errors very well. That is all we can do. This is where we ringfence the scope
of our App project.


Functions in core.py:
---------------------
OneCoinH.get_name()
OneCoinH.get_gender()
OneCoinH.get_length()
OneCoinH.get_hair()
OneCoinH.get_left_eye()
OneCoinH.get_right_eye()
OneCoinH.get_features()
OneCoinH.get_previous_idcodes()
OneCoinH.save_name()                  (including basic validity check)
OneCoinH.save_gender()
OneCoinH.save_length()                (including basic validity check)
OneCoinH.save_hair()
OneCoinH.save_left_eye()
OneCoinH.save_right_eye()
OneCoinH.save_features()              (including basic validity check)
OneCoinH.save_previous_idcodes()      (including basic validity check)
OneCoinH.upload_image()
OneCoinH.scale_image()
OneCoinH.rotate_image()
OneCoinH.position_image()
OneCoinH.contrast_image()
OneCoinH.brightness_image()
OneCoinH.saturation_image()
OneCoinH.save_image()
OneCoinH.generate_idcard()
OneCoinH.generate_idcode()
OneCoinH.approve_idcard()
OneCoinH.send_iddata()
OneCoinH.save_idcard()
OneCoinH.save_idcode()
OneCoinH.backup_data()
OneCoinH.restore_data()
OneCoinH.clear_all_data()
OneCoinH.show_manual()
OneCoinH.show_about()
OneCoinH.set_currency_symbol()         (Settings: If still necessary)
OneCoinH.set_language()                (Settings: Language)
OneCoinH.set_theme()                   (Settings: color theme app)
OneCoinH.load_new_pb()
OneCoinH.load_idcard_tp()
OneCoinH.send_payment_data()
OneCoinH.load_payment_data()
OneCoinH.show_proposal_advice()        (based on 'own coins back' slider and on the sort & shuffle selector)
OneCoinH.sort_proposal_tps()           (for manual tp selection in proposal)
OneCoinH.set_amount_tp()               (for manual tp selection in proposal)
OneCoinH.delete_payment_proposal()
OneCoinH.send_payment_proposal()
OneCoinH.load_payment_proposal()
OneCoinH.process_payment_user()
OneCoinH.process_payment_tp()
OneCoinH.show_balance()                (better name than cashflow)
OneCoinH.show_transaction_tens()       (show transactions in groups of 10)
OneCoinH.show_transaction()            (show single transaction)
OneCoinH.show_transaction_details_tp()
OneCoinH.get_pv()                      (gets the present value of historic amount, using the demurage formula)
OneCoinH.crosscheck_new_pb()           (new feature)
OneCoinH.show_blacklist()              (new feature)

Probably we will find a few functions on the way, but for now these seem to be the most important ones




Clarification:
--------------
"pb" means "personal blockchain" and is basically the ever increasing personal ledger of each user
participating in the OneCoinH system 

"tp" means "transaction partner" and are the persons the user does transactions with.

"idcard" is the 250x375 (bxh) png that show all the personal features and that is hashed in the hash that 
is called the IDCode.

"idcode" is the SHA256 hash of the png of the IDCard. The length of the hash is 256 bits (2^8=256 and 
because we have 256 bits of which each bit can be either 0 or 1, 256 bits have 512 (2x256) different 
combinations of 9 bits (9^2=512). 9^2 can be represented in a hexadecimal string of 32 characters. 
Hexadecimal means that only the characters '1234567890abcdef' are used 16=2^4 so we need to have 
2^(9-4) = 2^5 = 32 of these hexadecimal characters to represent the 256 bits hash. On the ID card we have
 32 boxes that each hold 8x8 = 64 pixels (64 = 2^6). To represent a 256 bits hash each pixel needs to have 
 2^(9-6) = 2^3 = 8 different colors to get a 256 bit hash represented in the box on the card. The boxes 
 are put on the card as a reference to the previous IDCard of that person. This way the latest (current) 
 card of that person connects all his previous IDCards together. In the pb something similars happens 
 because all previous IDCodes can be found in everyone's pb. So once you hash a new IDCode with a new 
 transaction, all IDCodes are connected to eachother in the latest hash.

"iddata" is a word that is used to refer to the combination of the specific idcard and idcode that is 
generated once the user accets to generate his new IDCard.

"data" is a referation to all the data a user collects. That is all his personal data plus all the IDCards 
and pb's he collects and some 'settings' data. Data like blacklist info is all generated based on the 
existing data, so doesn't need to be backed up and is therefore no part of the 'data' refered in the 
functions.

"blacklist" is the combination of coins and names of persons that showed errors when being cross checked.

===========================================================================
===========================================================================
git add functions.txt
git status
git commit -m "1st Upload"
git push


