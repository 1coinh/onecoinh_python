# Requirements.txt

It would be good to have a script that helps developers and people that want to run the python code 
on their machines (instead of using an APK on a smartphone) and want to know which supporting software 
they need to install (and which versions) to be able to do this. TODO?

# MAIN INSTALLATION

## Install Python:

```commandline
sudo apt install python3-pip
python3 --version
```


## Install Kivy:
_see more at: https://kivy.org/doc/stable-1.10.1/installation/installation-linux.html_
```commandline
sudo apt-get install python3-kivy
```


### Install Kivy’s virtual environment (necessary?):

```commandline
python3 -m venv venv
sudo apt-get install kivy-examples
```

### Install KivyMD:
_see more at: https://kivymd.readthedocs.io/en/1.1.1/changelog/index.html_
```commandline
pip install kivymd==1.1.1
```

### Install Git:
Update the package list:
```commandline
sudo apt update
sudo apt install git
```

#### Verify the installation by checking the version of Git:
```commandline
git --version
```

## Creating The APK

To create an APK, this seems to be the most promising method:
see more at: https://youtu.be/5waUjIYuxCs
Goto colab websit:
https://colab.research.google.com/drive/1tfvMqKSYQSnEnlk3d6dN4aeh718P2-Dj#scrollTo=b0mtXiPxp8OU

```commandline
pip install buildozer

sudo apt install -y git zip unzip openjdk-17-jdk \ 
    python3-pip autoconf libtool pkg-config zlib1g-dev \ 
    libncurses5-dev libncursesw5-dev libtinfo5 cmake \ 
    libffi-dev libssl-dev

pip install cython==0.29.33

sudo apt-get install -y \
    python3-pip build-essential git \
    python3 python3-dev ffmpeg \
    libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev \
    libsdl2-ttf-dev libportmidi-dev libswscale-dev \
    libavformat-dev libavcodec-dev zlib1g-dev

sudo apt-get install -y \
    libgstreamer1.0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good

sudo apt-get install build-essential \ 
    libsqlite3-dev sqlite3 bzip2 \ 
    libbz2-dev zlib1g-dev libssl-dev \ 
    openssl libgdbm-dev libgdbm-compat-dev \ 
    liblzma-dev libreadline-dev libncursesw5-dev \ 
    libffi-dev uuid-dev libffi7

sudo apt-get install libffi-dev
```

Now before running the next command upload your Python file

continue with:


```commandline
buildozer init
```

CHANGE THE REQUIREMENTS IN THE buildozer.spec FILE:
```
requirements = python3, kivy, kivymd, pillow    /    kivy==2.0.0, kivymd==0.104.1     /    kivy==2.3.0, kivymd==1.1.1  THIS NEEDS TO BE CHECKED. TODO!!!

buildozer -v android debug
```
(Don’t forget to click: 2x Y to accept rules...)


## ALTERNATIVE APK METHOD

Another possibility is to install the APK with buildozer on your Linux system. Buildozer only runs on Linux:
see also: https://youtu.be/yr7n0C2tspI

```commandline
sudo apt install python3-pip

pip3 install kivy

pip3 install buildozer

sudo apt update

sudo apt install -y git zip unzip openjdk-8-jdk python3-pip \
    autoconf libtool pkg-config zlib1g-dev \ 
    libncurses5-dev libncursesw5-dev libtinfo5 \ 
    cmake libffi-dev libssl-dev

pip3 install  Cython==0.29.19

export PATH=$PATH:~/.local/bin/

buildozer init
```

CHANGE THE REQUIREMENTS IN THE buildozer.spec FILE:
```
requirements = python3, kivy, kivymd, pillow    /    kivy==2.0.0, kivymd==0.104.1     /    kivy==2.3.0, kivymd==1.1.1  THIS NEEDS TO BE CHECKED. TODO!!!

buildozer -v android debug
```


## USING GIT

To participate in the GitLab 1coinh-goes-python project it is important to install 'Git' on your system. 
By using your terminal you can browse to the folder where you downloaded the 1CoinH-python files. 
In this directory you init git by using:

```commandline
git init
```

in your terminal. After that you can use:

```commandline
git status
```

to see the status of the software in your directory. New files will be first labelled as "Untracked files" to 
indicate they are invisible for the Gitlab repository. With various git instructions you can download the latest 
versions of the 1CoinH software from the Gitlab environment or upload any changes that you want to make to the 
latest version to the Gitlab environment. This is basically how open source software is developed. With the:

```commandline
git add .
```

instruction or with:

```commandline
git add <filename.ext> 
```
_(don't use quotation marks or brackets or anything around the filename, but do use the proper extension like .exe or so)_

you can indicate that the files in your directory are ready to be picked up by the Gitlab repository. Files that are 
'added' this way are now 'ready to be committed' to the Gitlab repository. By adding files and giving them a sort of 
'ready to commit' status, basically we create a group of files that can be sent to the Gitlab repository. This seems 
to be the reason for this first step. The next step is to 'commit' this entire group and connect a message to what the 
purpose of this groups' software changes is. The instruction to do this is:

```commandline
git commit -m "your message"
```

Committing is however not the last step. To actually push the files to the Gitlab repository, we need to use the 
'push' command. It looks like this:

```commandline
git push -u "https://gitlab.com/1coinh/onecoinh_python.git" main
```

Finally, to download the latest official version, you use the instruction:

```commandline
git pull
```

You can learn more about 'Git' in this video: https://youtu.be/OWaZXtgq28c?si=p9W5k0tFKn-41E5V

```commandline
git add requirements.txt
git status
git commit -m "1st Upload"
git push -f "https://gitlab.com/1coinh/onecoinh_python.git" main
```

