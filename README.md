# 1CoinH

Prepare the development environment:

```commandline
python -m venv venv
source venv/bin/activate

pip install -r requirements.txt
```

Run the application with:

```commandline
python onecoinh.py
```

## Release notes

- Nothing yet