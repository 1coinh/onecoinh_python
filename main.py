"""
One Coin per Hour Main Application.

see https://www.1coinh.org
"""
import os
import json
import pickle
import webbrowser
import random
from io import BytesIO

from kivy.core.image import Image as CoreImage
from kivy.uix.image import Image as DisplayImage

from datetime import datetime, timedelta
from typing import List

from kivy.config import Config
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.graphics import RoundedRectangle
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.image import Image
from kivymd.app import MDApp
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.pickers import MDDatePicker
from kivymd.uix.filemanager import MDFileManager
from kivymd.toast import toast
from kivymd.uix.list import OneLineListItem, ThreeLineRightIconListItem
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.list import IRightBodyTouch

from onecoinh import *
from onecoinh.utils import get_datetime, get_transaction_level, color_list

Config.set('kivy', 'exit_on_escape', '0')
Window.size = (360, 600)


def do_t1_t2_magic(t1: int, t2: int) -> str:
    if t2 <= t1:
        if t1 == 0:
            t2 = 1
        if t1 == 1:
            t2 = 2
        if t1 == 2:
            t2 = 5
        if t1 == 5:
            t2 = 10
        if t1 == 10:
            t2 = 20
        if t1 == 20:
            t2 = 50
        if t1 == 50:
            t2 = 100
        if t1 == 100:
            t2 = 200
        if t1 == 200:
            t2 = 500
        if t1 == 500:
            t2 = 1000
        if t1 == 1000:
            t2 = 2000
        if t1 == 2000:
            t2 = 5000
        if t1 == 5000:
            t2 = 10000
        if t1 >= 10000:
            t2 = 999999999
        if t2 > 10000:
            return "∞ U"
        else:
            return f"{t2} U"

def create_widget_image(identity: Identity):
    """
    Create Identity image
    that can be added to a Widget
    """
    # Image
    image = DisplayImage()
    im = CoreImage(BytesIO(identity.image), ext="jpg")
    image.texture = im.texture
    return image


class CustomWidget(OneLineListItem):
    pass


class TransactionEntryWidget(ThreeLineRightIconListItem):
    pass


class ImageContainer(IRightBodyTouch, MDBoxLayout):
    adaptive_width = True


class TransactionEntryDetailWidget(ThreeLineRightIconListItem):
    pass


class YourContainer2(IRightBodyTouch, MDBoxLayout):
    adaptive_width = True


class CustomWidget7(ThreeLineRightIconListItem):
    pass


class YourContainer3(IRightBodyTouch, MDBoxLayout):
    adaptive_width = True


class Window_Home(Screen):
    """ Main screen """


class Window_Pay(Screen):
    """ Payment screen """


class Window_Pay2(Screen):
    """ Payment screen 2 """


class Window_Pay3(Screen):
    """ Payment screen 3 """


class Window_Receive(Screen):
    """ Payment receive screen """


class Window_Receive2(Screen):
    """ Payment receive2 screen """


class Window_Receive3(Screen):
    """ Payment receive3 screen """


class Window_Receive4(Screen):
    """ Payment receive4 screen """


class Window_Receive5(Screen):
    """ Payment receive5 screen """


class Window_Receive6(Screen):
    """ Payment receive6 screen """


class Window_Cashbook(Screen):
    """ Cashbook screen """


class Window_Cashbook2(Screen):
    """ Cashbook screen """


class Window_Cashbook3(Screen):
    """ Cashbook screen """


class Window_Cashbook4(Screen):
    """ Cashbook screen """


class Window_Cashbook5(Screen):
    """ Cashbook screen """


class Window_Cashbook6(Screen):
    """ Cashbook screen """


class Window_Cashbook7(Screen):
    """ Cashbook screen """


class Window_User_Data(Screen):
    """ ID Card screen """


class Window_User_Data2(Screen):
    """ ID Card screen2 """


class Window_Settings(Screen):
    """ Settings screen """

    def set_color(self, actual_color):
        """ Set Colors for the individual elements on this screen """
        self.ids.userdata.md_bg_color = actual_color
        self.ids.language.md_bg_color = actual_color
        self.ids.screencolor.md_bg_color = actual_color
        self.ids.backup.md_bg_color = actual_color
        self.ids.restore.md_bg_color = actual_color
        self.ids.clear_data.md_bg_color = actual_color
        self.ids.settingsbar.icon_color = actual_color

    def on_enter(self):
        """ Bins click event when entering the settings screen """
        Window.bind(on_keyboard=self.back_click)

    def on_pre_leave(self):
        """ Before leaving the screen, unbind the click event """
        Window.unbind(on_keyboard=self.back_click)

    def back_click(self, window, key, keycode, *args):
        """ Action when user clicks back button """
        if key == 27:
            print('go home')
            WindowManager.current = 'Home'  # doesn't work...


class Window_About(Screen):
    """ About screen """


class Window_Select_Color(Screen):
    """ Color selection screen """


class Window_Select_Language(Screen):
    """ Language selection screen """
    def set_color(self, actual_color):
        """ Set Colors for the individual elements on this screen """
        self.ids.languagebar.icon_color = actual_color
        self.ids.de.text_color = actual_color
        self.ids.en.text_color = actual_color
        self.ids.es.text_color = actual_color
        self.ids.fr.text_color = actual_color
        self.ids.it.text_color = actual_color
        self.ids.ne.text_color = actual_color
        self.ids.po.text_color = actual_color
        self.ids.pg.text_color = actual_color
        self.ids.ru.text_color = actual_color


class Window_Backup(Screen):
    """ Backup creation screen """


class Window_Restore(Screen):
    """ Backup restore screen """


class Window_Clear_Data(Screen):
    """ Clear data screen """


class Window_Name(Screen):
    """ Name enter screen """
    def set_color(self, actual_color):
        """ Set Colors for the individual elements on this screen """
        self.ids.namebar.icon_color = actual_color
        self.ids.namefield.theme_text_color = "Custom"
        self.ids.namefield.text_color = actual_color
        self.ids.namefield.line_color_normal = actual_color
        self.ids.namefield.line_color_focus = actual_color
        self.ids.namefield.hint_text_color_normal = actual_color
        self.ids.namefield.hint_text_color_focus = actual_color
        self.ids.label1.theme_text_color = "Custom"
        self.ids.label1.text_color = actual_color
        self.ids.label2.theme_text_color = "Custom"
        self.ids.label2.text_color = actual_color


class Window_Height(Screen):
    """ Define height screen """


class Window_Special_Features(Screen):
    """ Enter extra feature information screen """


class Window_Set_Image(Screen):
    """ Set Image screen """
    # rect = None
    # circle = None
    # line = None
    # def __init__(self, **kwargs):
    #     super(Window_Set_Image(), self).__init__(**kwargs)
    #     self.init_shapes()
    # def init_shapes(self):
    #     self.rect = Rectangle(bg_color=(244 / 255, 67 / 255, 54 / 255, 1))
    #     self.circle = Ellipse(bg_color=(244 / 255, 67 / 255, 54 / 255, 1))
    #     self.line = Line(bg_color=(244 / 255, 67 / 255, 54 / 255, 1))


class Content(BoxLayout):
    """ General content box """


class WindowManager(ScreenManager):
    """ Window manager, wrapper for screens """


class OneCoinHApp(MDApp):
    """ Main application """
    identity: Identity
    library: OneCoinH = None
    language_selected = "en"
    color_selected = "Amber"
    user_gender = "choose"
    user_meters = 0
    user_feet = 0
    user_inches = 0
    user_height_switch = False
    user_hair = "choose"
    user_left_eye = "choose"
    user_right_eye = "choose"
    user_photo_path = ""
    user_birthday_day = 1
    user_birthday_month = 1
    user_birthday_year = 1900

    sl_hor_pos = 0
    sl_ver_pos = 0
    sl_scale = 100
    sl_color = 0
    sl_bright = 0
    sl_rotation = 0
    my_image = None
    img_width = 0
    img_height = 0
    pos_x = 0
    pos_y = 0

    # Data Personal Blockchains CashBook
    pb_id: List[Identity] = []

    # Data Personal Blockchain Receiver, Teun van Sambeek is the receiver
    pb_person = [['Teun van Sambeek', '123456dea673947d123456dea673947d123456dea673947d123456dea673947d', 13586.45, 4],
                 ['Alf van de Akker', '3386ad8026se09843386ad8026se09843386ad8026se09843386ad8026se0984', 6235.60, 3],
                 ['Peter Deacon', '999f44dae4b4b58a909240dae5b55835909240d35685b38a909240dae5b5b38a', 3474.75, 7],
                 ['Johnny Roteng', '789a46dae9bdb68a909240dae5b5838a609240dae553b38a909240dae5b5b384', 36.54, 2],
                 ['Hasan Djokona', '326d41dae8beb78a909240dae5b3538a809240dae5b5b38a909240dae5b8b385', 7.48, 3],
                 ['Heidi Skleidoni', '474e47dae7bab35a909240dae365b38a569240dae5b5b38a909240d43565b386', 3448.57, 5],
                 ['Verdano Massa', '615c45dae6bdb886905640dae895b38a9052405685b5b38a90564568e5b5b387', 654.11, 4],
                 ['Tubinio Calon', '776a44dae5beb33a638240da5535b38a909680da35b5b38a984568dae5b56882', 30.00, 4],
                 ['Theodore Meekers', '837d45dae4ba3358909240dae5b5838a909258dae8b5b384568240dae558b384', 523.32, 3],
                 ['Kenau Rondono', '244e467ae2bd838a989240dae5b5b368909240dae565b385609240dae365b388', 58.47, 6],
                 ['Paulo Messiar', '847a408ae1ba3888609240da58b5b38a5092456ae555b358909240da86b5b385', 1.58, 2],
                 ['Paul Vengard', '008d409a15be9386809240dae5b5b38a968540dae5b8b88a9092406585b5b383', 0.70, 2],
                 ['Quint Wezelar', '189e400125bd738a589240da65b5b38a903240dae5b5638a909240da85b5b389', 5532.82, 3],
                 ['Bruce Wilson', '3b6f629875c3897a90987259c7987e98c7879a8755098a480092405688678351', 72.18, 4],
                 ['Story Mothers', '833c4067e5ba538a906840da68b5b38a909240dae556b38a909240d555b5b383', 72.18, 4],
                 ['Leo Di Amdani', 'a49d4042e5bc435680956835e5b5b38a909240dae685b38a9092435885b5b381', 27.36, 6],
                 ['Marty VanderLee', 'b83a4067e5ba538a909245dae5b5b38a909240d585b5b38a9092405ae5b5b388', 235.23, 1],
                 ['Donna Partola', '958e4054e536b38a9092405ae5b5b38a539240da8356838a909240dae5458389', 10.54, 2],
                 ['Amy Wevenburg', '1b43128de2ab8d881b43128de2ab8d881b43128de2ab8d881b43128de2ab8d88', 405.88, 3],
                 ['Coby Messia', 'e56e4081e5bc73568092408ae5b5638a909240d8e5b5b38a9092658ae5b5b383', 2854.96, 7],
                 ['Stefano Caldo', 'd37f4099e5b4438a909240d56558b35a9093568ae5b5b38a909580dae5b5b380', 0.20, 3],
                 ['Boris Hooning', 'a85e4028e5b55383568240dae8b5b386809246dae5b5b38a954680dae5b5b383', 48.73, 2],
                 ['Walter Peacock', '426a4067e5b68368909240d35538b385609268dae5b5b38a309240dae5b5b382', 6.11, 0],
                 ['Victor Woodorn', '987c4056e5b8b88a90924538e5b3568a90954358e5b5b356905680dae5b5b381', 784.22, 4],
                 ['Roger Snowplow', '238d4084e5b9b38a909240dae5b5b38a556240d5e5b5588a456840dae5b5b387', 1365.98, 2],
                 ['Casper Hansen', '045a4062e536578a9092438ae5b5b38a989240da68b5b38a909240dae5b5b388', 48.66, 6]]
    pb_items = len(pb_person)
    ##############################
    # Snapshot data from QR Code #
    ##############################
    # Bruce = Payer
    snapshot_payer = ['Bruce', '3b6f62', 14305.43, 5, False, 0]
    snapshot_person = [['Teun', '123456', 17.45, 2, False, 0],
                       ['John', 'a2cd64', 285.48, 5, False, 1],
                       ['Pete', 'ab0c46', 1275.62, 7, False, 2],
                       ['Mary', 'ab9f38', 255.49, 2, False, 3],
                       ['Alf', '3386ad', 235.60, 3, False, 4],
                       ['Harold', '1477bc', 5.11, 4, False, 5],
                       ['Peter', '6b61ea', 3255.07, 3, False, 6],
                       ['Milly', 'a7a8c1', 95.04, 2, False, 7],
                       ['Cory', '9bd4d7', 505.95, 3, False, 8],
                       ['Snow', '7b00a6', 105.87, 4, False, 9],
                       ['Bert', '6b2795', 1805.71, 4, False, 10],
                       ['Vila', 'a49853', 905.60, 1, False, 11],
                       ['Fred', '2b5473', 0.11, 0, False, 12],
                       ['Amy', '1b4312', 405.88, 3, False, 13]]
    snapshot_items = len(snapshot_person)
    indexlist = [-1, -1, -1, -1, -1]
    amountlist = [0, 0, 0, 0, 0]
    otherslist = [0, 0, 0, 0, 0]
    payerpart = 0
    sortedlist = snapshot_person
    sortedlist2 = snapshot_person
    other_manual = -1
    other_manual0 = -1
    other_manual1 = -1
    other_manual2 = -1
    other_manual3 = -1
    other_manual4 = -1
    amount_goal = 0
    owncoins = 0  # current value
    coinscreated_past_value = 0  # past value
    trans = []
    CB2_ItemsPerPage = 10
    CB2_CurrentPage = 0
    user = 0
    wallet = [0] * 50
    paid = [0] * 50
    received = [0] * 50
    selected_transaction = -1
    from27 = 2
    max_search_results = 50

    def __init__(self):
        """ Application start """
        super().__init__()
        with open('./i18n.json') as i18n_file:
            self.translation = json.load(i18n_file)
        self.text_color = None
        self.theme_text_color = None
        self.manager_open = False
        self.file_manager = MDFileManager(
            exit_manager=self.exit_manager,
            select_path=self.select_path
        )

    def build(self):
        """ Build the user interface """
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = self.color_selected
        self.theme_cls.accent_palette = self.color_selected
        self.theme_text_color = "Custom"
        try:
            self.text_color = color_list[self.color_selected]
        except KeyError:
            self.text_color = color_list['amber']

        return Builder.load_file('window_manager.kv')

    def set_name(self, name: str):
        self.root.get_screen('User_Data').ids.fullname.text = name
        self.root.get_screen('Name').ids.namefield.text = name

    def get_name(self):
        if self.library.identity.name is not None:
            return self.library.identity.name
        return ""

    def on_start(self):
        self.library = OneCoinH()
        if self.library.identity is not None:
            self.identity = self.library.identity
            if self.identity.name is not None:
                self.set_name(self.identity.name)
        # load the sample data
        with open("data/example_pb.pickle", 'rb') as f:
            self.pb_id = pickle.load(f)

    def go_home(self):
        self.root.current = "Home"
        self.root.transition.direction = "right"

    def go_pay(self):
        self.root.current = "Pay"
        self.root.transition.direction = "left"

    def go_pay2_r(self):
        self.root.current = "Pay2"
        self.root.transition.direction = "right"

    def go_receive(self):
        self.root.current = "Receive"
        self.root.transition.direction = "left"

    def get_tli(self, pay0: int) -> int:
        """ Get the transaction level indicator """
        # pay
        pay1 = get_transaction_level(self.paid[pay0])
        # receive
        rec1 = get_transaction_level(self.received[pay0])
        return (pay1 * 10) + rec1

    def go_cashbook(self):
        """ Read simulation data """
        screen = "Cashbook"
        ids = self.root.get_screen(screen).ids
        f = open("transactions.txt", "r")
        for i in range(0, 6171):
            ss = f.readline()
            sdat = get_datetime(ss)
            coins = int(ss[19:24])
            receiver = int(ss[27:29])
            amount = float(ss[30:38])
            payer = int(ss[39:42])
            pp = float(ss[43:50])
            ot1 = int(ss[51:54])
            ota1 = float(ss[55:62])
            ot2 = int(ss[63:66])
            ota2 = float(ss[67:74])
            ot3 = int(ss[75:78])
            ota3 = float(ss[79:86])
            ot4 = int(ss[88:90])
            ota4 = float(ss[91:98])
            ot5 = int(ss[99:102])
            ota5 = float(ss[103:109])
            tli0 = ss[112:114]
            tli1 = ss[116:118]
            tli2 = ss[120:122]
            tli3 = ss[124:126]
            tli4 = ss[128:130]
            tli5 = ss[132:134]
            description = ss[135:]

            if self.user == receiver or self.user == payer:
                new_elem = [
                    sdat.day,
                    sdat.month,
                    sdat.year,
                    sdat.hour,
                    coins,
                    receiver,
                    payer,
                    int(ss[51:54]),  # other 1
                    int(ss[63:66]),  # other 2
                    int(ss[75:78]),  # other 3
                    int(ss[88:90]),  # other 4
                    int(ss[99:102]),  # other 5
                    pp,
                    float(ss[55:62]),  # other 1 amount
                    float(ss[67:74]),  # other 2 amount
                    float(ss[79:86]),  # other 3 amount
                    float(ss[91:98]),  # other 4 amount
                    float(ss[103:109]),  # other 5 amount
                    i,
                    tli0,
                    tli1,
                    tli2,
                    tli3,
                    tli4,
                    tli5,
                    description]
                self.trans.append(new_elem)
        f.close()
        # Reverse the transaction list of user, so the newest=0,etc
        self.trans.reverse()

        # END READ SIMULATION DATA
        self.owncoins = 0

        track_date = datetime(2022, 1, 1, 0)
        counter = 0
        spent_own_coins = 0
        coins_from_others = 0
        spent_from_others = 0
        while track_date.date() < datetime.now().date():
            self.owncoins = (self.owncoins * 0.99995) + 1
            spent_own_coins = (spent_own_coins * 0.99995)
            coins_from_others = (coins_from_others * 0.99995)
            spent_from_others = (spent_from_others * 0.99995)
            counter += 1
            track_date += timedelta(hours=1)  # keep adding hours until we reach today

            # Check all transactions. Because all transactions are checked each time,
            # the depreciation still works, even when counter and trans[i] go in
            # opposite directions in time

            for i in range(0, len(self.trans)):
                # if ((self.trans[i][4] == counter) and ((self.trans[i][5] == user) or (self.trans[i][6] == user))):
                if self.trans[i][4] == counter:
                    if self.trans[i][5] == self.user:  # user = receiver
                        coins_from_others += (
                                self.trans[i][12] + self.trans[i][13] + self.trans[i][14] +
                                self.trans[i][15] + self.trans[i][16] + self.trans[i][17]
                        )
                    if self.trans[i][6] == self.user:  # user = payer
                        spent_own_coins += self.trans[i][12]
                        spent_from_others += (
                                self.trans[i][13] + self.trans[i][14] + self.trans[i][15] +
                                self.trans[i][16] + self.trans[i][17]
                        )

        self.coinscreated_past_value = counter
        coinscreated_present_value = self.owncoins

        coins_available = (
                coinscreated_present_value +
                coins_from_others -
                (spent_own_coins + spent_from_others)
        )
        # fill screen
        ids.currenthour.text = f"{self.i18n('available_per')} " \
                               f"{track_date.strftime('%x')}"
        ids.label10.text = f"{self.coinscreated_past_value:.2f} U "
        ids.label40.text = f"{coinscreated_present_value:.2f} U "
        ids.label50.text = f"{spent_own_coins:.2f} U "
        ids.label70.text = f"{coins_from_others:.2f} U "
        ids.label80.text = f"{spent_from_others:.2f} U "
        ids.label90.text = f"{coins_available:.2f} U "
        # Move to Cashbook Page
        self.root.current = "Cashbook"
        self.root.transition.direction = "left"

    def go_cashbook_r(self):
        self.root.current = "Cashbook"
        self.root.transition.direction = "right"

    def get_cashbook_entries(self):
        """ Get the line items to be shown in the cashbook list """
        screen = "Cashbook2"
        ids = self.root.get_screen(screen).ids
        ids.cashbook_entries_container.clear_widgets()

        for i in range(self.CB2_CurrentPage, self.CB2_CurrentPage + self.CB2_ItemsPerPage):
            trans_date = datetime(self.trans[i][2], self.trans[i][1], self.trans[i][0], self.trans[i][3])

            widget = TransactionEntryWidget()
            widget.id = str(i)
            widget.theme_text_color = "Custom"

            amt = round((self.trans[i][12] + self.trans[i][13] + self.trans[i][14] + self.trans[i][15] +
                         self.trans[i][16] + self.trans[i][17]), 2)
            if self.trans[i][6] == self.user:  # user = payer
                widget.text_color = (1, 0, 0, 1)  # red
                amt = -amt
            else:  # user = receiver
                widget.text_color = (1, 1, 1, 1)  # white
            amt0 = amt  # past value
            shc = self.trans[i][4]
            # because the current value is calculated each time again,
            # it doesn't matter that trans[] foes backward in time
            for j in range(shc, self.coinscreated_past_value):  # shc to now
                amt = amt * 0.99995
            widget.text = f"{amt:.2f} U (past value: {amt0:.2f} U)"
            widget.secondary_text_color = (
                1, 1, 1, 1
            )  # white
            if self.trans[i][0] == self.user:  # user = payer
                _action = "Paid"
                _identity: Identity = self.pb_id[self.trans[i][5]]
            else:
                _action = "Received"
                _identity: Identity = self.pb_id[self.trans[i][6]]

            widget.secondary_text = f"{_action} on: {trans_date.strftime('%x')}"
            widget.tertiary_text = f"{_identity.name}"

            widget.ids.image_container.add_widget(create_widget_image(_identity))
            ids.cashbook_entries_container.add_widget(widget)

    def go_cashbook2(self):
        """ Create transaction list """
        self.CB2_CurrentPage = 0
        self.get_cashbook_entries()
        self.root.current = "Cashbook2"
        self.root.transition.direction = "left"

    def go_first(self):
        """
        Paginate through list,
        go to first n items given the page size
        """
        self.CB2_CurrentPage = 0
        self.get_cashbook_entries()

    def go_previous(self):
        """
        Paginate through list,
        go back the number of items defined
        in the page size
        """
        self.CB2_CurrentPage -= self.CB2_ItemsPerPage
        if self.CB2_CurrentPage < 0:
            self.CB2_CurrentPage = 0
        self.get_cashbook_entries()

    def go_next(self):
        """
        Paginate through list,
        go to the last number of items defined
        in the page size
        """
        self.CB2_CurrentPage += self.CB2_ItemsPerPage
        mx = int(len(self.trans) / self.CB2_ItemsPerPage)
        if self.CB2_CurrentPage > mx:
            self.CB2_CurrentPage = mx
        self.get_cashbook_entries()

    def go_last(self):
        """
        Paginate through list,
        go back the number of items defined
        in the page size
        """
        self.CB2_CurrentPage = int(len(self.trans) / self.CB2_ItemsPerPage)
        self.get_cashbook_entries()

    def go_cashbook2_r(self):
        if self.from27 == 7:
            self.root.current = "Cashbook7"
            self.root.transition.direction = "right"
        else:
            self.root.current = "Cashbook2"
            self.root.transition.direction = "right"

    def select_tp(self, key: str):
        i = int(key)
        screen = "Cashbook3"
        ids = self.root.get_screen(screen).ids

        self.selected_transaction = i
        amt = self.trans[i][12] + self.trans[i][13] + self.trans[i][
            14] + self.trans[i][15] + self.trans[i][16] + self.trans[i][17]
        oms = self.trans[i][25][:-2]  # remove "\n"
        if self.trans[i][6] == self.user:  # user = payer
            ids.label2.text_color = (1, 0, 0, 1)  # red
            ids.label12.text_color = (1, 0, 0, 1)  # red
            ids.label32.text_color = (1, 0, 0, 1)  # red
            ids.label42.text_color = (1, 0, 0, 1)  # red
            ids.label52.text_color = (1, 0, 0, 1)  # red
            ids.label62.text_color = (1, 0, 0, 1)  # red
            ids.label72.text_color = (1, 0, 0, 1)  # red
            ids.label91.text_color = (1, 0, 0, 1)  # red
            ids.label92.text_color = (1, 0, 0, 1)  # red
            sign = "-"
            amt = -amt
            oms = f"Bought: {oms}"
            name1 = self.pb_id[self.trans[i][5]].name
        else:  # user = receiver
            ids.label2.text_color = (1, 1, 1, 1)  # white
            ids.label12.text_color = (1, 1, 1, 1)  # white
            ids.label32.text_color = (1, 1, 1, 1)  # white
            ids.label42.text_color = (1, 1, 1, 1)  # white
            ids.label52.text_color = (1, 1, 1, 1)  # white
            ids.label62.text_color = (1, 1, 1, 1)  # white
            ids.label72.text_color = (1, 1, 1, 1)  # white
            ids.label91.text_color = (1, 1, 1, 1)  # white
            ids.label92.text_color = (1, 1, 1, 1)  # white
            sign = ""
            oms = f"{self.i18n('sold')}: {oms}"
            name1 = self.pb_id[self.trans[i][6]].name
        ids.label1.text = oms
        ids.label2.text = f"{self.i18n('amount')}: {amt:.2f} U "
        # Transaction Level Indicators
        mut = [False, False, False, False, False, False]
        if i < len(self.trans):  # see who are mutual
            receiver = self.trans[i][5]
            payer = self.trans[i][6]
            ot1 = self.trans[i][7]
            ot2 = self.trans[i][8]
            ot3 = self.trans[i][9]
            ot4 = self.trans[i][10]
            ot5 = self.trans[i][11]
            for j in range(i + 1, len(self.trans)):
                if payer == self.trans[j][5] or payer == self.trans[j][6]:
                    # payer = part of the previous transaction
                    mut[0] = mut[0] or (
                            receiver == self.trans[j][5] or receiver == self.trans[j][6]
                    )
                    mut[1] = mut[1] or (
                            ot1 == self.trans[j][5] or ot1 == self.trans[j][6]
                    )
                    mut[2] = mut[2] or (
                            ot2 == self.trans[j][5]) or (ot2 == self.trans[j][6]
                                                         )
                    mut[3] = mut[3] or (
                            ot3 == self.trans[j][5]) or (
                                     ot3 == self.trans[j][6]
                             )
                    mut[4] = mut[4] or (ot4 == self.trans[j][5]) or (ot4 == self.trans[j][6])
                    mut[5] = mut[5] or (ot5 == self.trans[j][5]) or (ot5 == self.trans[j][6])
        if mut[0]:
            ids.label10.text_color = (0, 1, 0, 1)  # green
            ss0 = f"[u]{self.trans[i][19]}[/u]"
        else:
            ids.label10.text_color = (0.5, 0.5, 0.5, 1)  # gray
            ss0 = self.trans[i][19]
        if mut[1]:
            ids.label30.text_color = (0, 1, 0, 1)  # green
            ss1 = f"[u]{self.trans[i][20]}[/u]"
        else:
            ids.label30.text_color = (0.5, 0.5, 0.5, 1)  # gray
            ss1 = self.trans[i][20]
        if mut[2]:
            ids.label40.text_color = (0, 1, 0, 1)  # green
            ss2 = f"[u]{self.trans[i][21]}[/u]"
        else:
            ids.label40.text_color = (0.5, 0.5, 0.5, 1)  # gray
            ss2 = self.trans[i][21]
        if mut[3]:
            ids.label50.text_color = (0, 1, 0, 1)  # green
            ss3 = f"[u]{self.trans[i][22]}[/u]"
        else:
            ids.label50.text_color = (0.5, 0.5, 0.5, 1)  # gray
            ss3 = self.trans[i][22]
        if mut[4]:
            ids.label60.text_color = (0, 1, 0, 1)  # green
            ss4 = f"[u]{self.trans[i][23]}[/u]"
        else:
            ids.label60.text_color = (0.5, 0.5, 0.5, 1)  # gray
            ss4 = self.trans[i][23]
        if mut[5]:
            ids.label70.text_color = (0, 1, 0, 1)  # green
            ss5 = f"[u]{self.trans[i][24]}[/u]"
        else:
            ids.label70.text_color = (0.5, 0.5, 0.5, 1)  # gray
            ss5 = self.trans[i][24]
        ids.label10.text = ss0
        if self.trans[i][13] > 0:
            ids.label30.text = ss1
            ids.label31.text = self.pb_id[self.trans[i][7]].name
            ids.label32.text = f"{sign}{self.trans[i][13]:.2f} U "
        else:
            ids.label30.text = ""
            ids.label31.text = ""
            ids.label32.text = ""
        if self.trans[i][14] > 0:
            ids.label40.text = ss2
            ids.label41.text = self.pb_id[
                self.trans[i][8]].name
            ids.label42.text = f"{sign}{self.trans[i][14]:.2f} U "
        else:
            ids.label40.text = ""
            ids.label41.text = ""
            ids.label42.text = ""
        if self.trans[i][15] > 0:
            ids.label50.text = ss3
            ids.label51.text = self.pb_id[
                self.trans[i][9]].name
            ids.label52.text = f"{sign}{self.trans[i][15]:.2f} U "
        else:
            ids.label50.text = ""
            ids.label51.text = ""
            ids.label52.text = ""
        if self.trans[i][16] > 0:
            ids.label60.text = ss4
            ids.label61.text = self.pb_id[self.trans[i][10]].name
            ids.label62.text = f"{sign}{self.trans[i][16]:.2f} U "
        else:
            ids.label60.text = ""
            ids.label61.text = ""
            ids.label62.text = ""
        if self.trans[i][17] > 0:
            ids.label70.text = ss5
            ids.label71.text = self.pb_id[
                self.trans[i][11]].name
            ids.label72.text = f"{sign}{self.trans[i][17]:.2f} U "
        else:
            ids.label70.text = ""
            ids.label71.text = ""
            ids.label72.text = ""
        ids.label11.text_color = (1, 1, 1, 1)  # white
        ids.label31.text_color = (1, 1, 1, 1)  # white
        ids.label41.text_color = (1, 1, 1, 1)  # white
        ids.label51.text_color = (1, 1, 1, 1)  # white
        ids.label61.text_color = (1, 1, 1, 1)  # white
        ids.label71.text_color = (1, 1, 1, 1)  # white
        ids.label11.text = name1
        ids.label12.text = sign + f"{self.trans[i][12]:.2f} U "
        ids.label92.text = f"{amt:.2f} U "
        self.root.current = screen
        self.root.transition.direction = "left"

    def go_cashbook3_r(self):
        self.root.current = "Cashbook3"
        self.root.transition.direction = "right"

    def go_cashbook4(self):
        """ Create fourth cashbook page """
        screen = "Cashbook4"
        ids = self.root.get_screen(screen).ids
        ids.container4.clear_widgets()
        # Add listitems, use self.selected_transaction
        i = self.selected_transaction
        # Add payer or receiver
        person = [0, 0, 0, 0, 0, 0]
        if self.trans[i][6] == self.user:  # user = payer
            clr = (1, 0, 0, 1)  # red
            sign = "-"
            person[0] = self.trans[i][5]
            ttx = "Paid to: "
        else:  # user = receiver
            sign = ""
            clr = (1, 1, 1, 1)  # white
            person[0] = self.trans[i][6]
            ttx = "Paid by: "
        for j in range(1, 6):
            person[j] = self.trans[i][j + 6]
        for j in range(0, 6):
            amt = round((self.trans[i][12 + j]), 2)
            if amt > 0:
                widget = TransactionEntryDetailWidget()
                widget.id = str(person[j])
                widget.theme_text_color = "Custom"
                widget.secondary_theme_text_color = "Custom"
                widget.tertiary_theme_text_color = "Custom"
                widget.text_color = (1, 1, 1, 1)  # white
                widget.text = self.pb_id[person[j]].name
                widget.secondary_text_color = clr
                widget.secondary_text = sign + "{:.2f}".format(amt) + " U"
                widget.tertiary_text_color = (1, 1, 1, 1)  # white
                widget.tertiary_text = ttx + self.pb_id[person[0]].name
                widget.ids.image_container.add_widget(
                    create_widget_image(self.pb_id[person[j]])
                )
                ids.container4.add_widget(widget)
        self.root.current = screen
        self.root.transition.direction = "left"

    def select_tp2(self, key: str):
        screen = "Cashbook5"
        i = int(key)
        ids = self.root.get_screen(screen).ids
        # Image
        ids.image_container.clear_widgets()
        ids.image_container.add_widget(create_widget_image(self.pb_id[i]))

        ids.label2.text = f"  {self.pb_id[i].name}"
        ids.label4.text = f"  {self.pb_id[i].dob.strftime('%Y-%m-%d')}"
        ids.label6.text = f"  {self.i18n(self.pb_id[i].gender)}"
        ids.label8.text = f"  {self.pb_id[i].height}"
        ids.label10.text = f"  {self.i18n(self.pb_id[i].hair)}"
        ids.label12.text = f"  {self.i18n(self.pb_id[i].eyes[0])}"
        ids.label14.text = f"  {self.i18n(self.pb_id[i].eyes[1])}"
        ids.label16.text = f"  Some Features"
        self.root.current = screen
        self.root.transition.direction = "left"

    def go_cashbook4_r(self):
        self.root.current = "Cashbook4"
        self.root.transition.direction = "right"

    def go_cashbook5_r(self):
        self.root.current = "Cashbook5"
        self.root.transition.direction = "right"

    def set_date1(self, pressed, date1):
        """ Set the default date for the DatePicker """
        # Set the DatePicker variables, functions and start it
        date_dialog = MDDatePicker(
            title=self._('select_date1').upper(),
            title_input=self._('type_date1').upper(),
            helper_text=self._('wrong_date'),
            min_year=2022,
            max_date=datetime.today(), day=1, month=1, year=2024)
        date_dialog.bind(on_save=self.on_save_date1, on_cancel=self.on_cancel_date1)
        date_dialog.open()

    def on_save_date1(self, instance, value, date_range):
        """ Save the date1 """
        picker_day = int(str(value)[8:])
        picker_month = int(str(value)[5:7])
        picker_year = int(str(value)[0:4])
        s_day = str(picker_day)
        if picker_day < 10:
            s_day = "0" + s_day
        s_month = str(picker_month)
        if picker_month < 10:
            s_month = "0" + s_month
        s_year = str(picker_year)
        s_date = s_day + "-" + s_month + "-" + s_year
        self.root.get_screen('Cashbook6').ids.date1.text = s_date

    def on_cancel_date1(self, instance, value):
        pass

    def set_date2(self, pressed, date2):
        """ Set the default date for the DatePicker """
        # Set the DatePicker variables, functions and start it
        date_dialog = MDDatePicker(
            title=self._('select_date2').upper(),
            title_input=self._('type_date2').upper(),
            helper_text=self._('wrong_date'),
            min_year=2022,
            max_date=datetime.today(), day=1, month=1, year=2024)
        date_dialog.bind(on_save=self.on_save_date2, on_cancel=self.on_cancel_date2)
        date_dialog.open()

    def on_save_date2(self, instance, value, date_range):
        """ Save the date2 """
        picker_day = int(str(value)[8:])
        picker_month = int(str(value)[5:7])
        picker_year = int(str(value)[0:4])
        s_day = str(picker_day)
        if picker_day < 10:
            s_day = "0" + s_day
        s_month = str(picker_month)
        if picker_month < 10:
            s_month = "0" + s_month
        s_year = str(picker_year)
        s_date = s_day + "-" + s_month + "-" + s_year
        self.root.get_screen('Cashbook6').ids.date2.text = s_date

    def on_cancel_date2(self, instance, value):
        pass

    def dropdown1(self):
        self.menu_list = [
            {
                "viewclass": "OneLineListItem",
                "text": "0 U",
                "on_release": lambda x="0 U": self.test1("0")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "1 U",
                "on_release": lambda x="1 U": self.test1("1")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "2 U",
                "on_release": lambda x="2 U": self.test1("2")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "5 U",
                "on_release": lambda x="5 U": self.test1("5")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "10 U",
                "on_release": lambda x="10 U": self.test1("10")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "20 U",
                "on_release": lambda x="20 U": self.test1("20")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "50 U",
                "on_release": lambda x="50 U": self.test1("50")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "100 U",
                "on_release": lambda x="100 U": self.test1("100")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "200 U",
                "on_release": lambda x="200 U": self.test1("200")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "500 U",
                "on_release": lambda x="500 U": self.test1("500")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "1000 U",
                "on_release": lambda x="1000 U": self.test1("1000")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "2000 U",
                "on_release": lambda x="2000 U": self.test1("2000")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "5000 U",
                "on_release": lambda x="5000 U": self.test1("5000")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "10000 U",
                "on_release": lambda x="10000 U": self.test1("10000")
            }
        ]
        self.menu = MDDropdownMenu(
            caller=self.root.get_screen('Cashbook6').ids.amt1,
            items=self.menu_list,
            width_mult=4
        )
        self.menu.open()

    def test1(self, no):
        self.menu.clear_widgets()
        self.root.get_screen('Cashbook6').ids.amt1.text = f"{no} U"
        t1 = int(no)
        ss2 = self.root.get_screen('Cashbook6').ids.amt2.text[:-2]
        if ss2 == "∞":
            ss2 = "999999999"
        t2 = int(ss2)
        ss1 = do_t1_t2_magic(t1, t2)
        self.root.get_screen('Cashbook6').ids.amt2.text = ss1

    def dropdown2(self):
        self.menu_list = [
            {
                "viewclass": "OneLineListItem",
                "text": "1 U",
                "on_release": lambda x="1 U": self.test2("1")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "2 U",
                "on_release": lambda x="2 U": self.test2("2")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "5 U",
                "on_release": lambda x="5 U": self.test2("5")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "10 U",
                "on_release": lambda x="10 U": self.test2("10")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "20 U",
                "on_release": lambda x="20 U": self.test2("20")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "50 U",
                "on_release": lambda x="50 U": self.test2("50")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "100 U",
                "on_release": lambda x="100 U": self.test2("100")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "200 U",
                "on_release": lambda x="200 U": self.test2("200")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "500 U",
                "on_release": lambda x="500 U": self.test2("500")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "1000 U",
                "on_release": lambda x="1000 U": self.test2("1000")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "2000 U",
                "on_release": lambda x="2000 U": self.test2("2000")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "5000 U",
                "on_release": lambda x="5000 U": self.test2("5000")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "10000 U",
                "on_release": lambda x="10000 U": self.test2("10000")
            },
            {
                "viewclass": "OneLineListItem",
                "text": "∞ U",
                "on_release": lambda x="∞ U": self.test2("999999999")
            }
        ]
        self.menu = MDDropdownMenu(
            caller=self.root.get_screen('Cashbook6').ids.amt2,
            items=self.menu_list,
            width_mult=4
        )
        self.menu.open()

    def test2(self, no):
        self.menu.clear_widgets()
        self.root.get_screen('Cashbook6').ids.amt2.text = str(no)+" U"
        t1 = int(no)
        t2 = int(self.root.get_screen('Cashbook6').ids.amt1.text[:-2])
        ss1 = do_t1_t2_magic(t1, t2)
        self.root.get_screen('Cashbook6').ids.amt1.text = ss1

    def search_txt(self):
        screen = "Cashbook7"
        ids = self.root.get_screen(screen).ids
        ids.cashbook_entries_container.clear_widgets()
        self.from27 = 7

        for i in range(0, len(self.trans)):
            fits = False
            amount = self.trans[i][12] + self.trans[i][13] + self.trans[i][14] + self.trans[i][15] + self.trans[i][16] + self.trans[i][17]
            description = self.trans[i][25].upper()
            if self.trans[i][5] == self.user:
                nm0 = self.pb_id[self.trans[i][6]].name.upper()
            else:
                nm0 = self.pb_id[self.trans[i][5]].name.upper()
            nm1 = ""
            nm2 = ""
            nm3 = ""
            nm4 = ""
            nm5 = ""
            if ((self.trans[i][7] > -1) and (self.trans[i][13]>0)): nm1 = self.pb_id[self.trans[i][7]].name.upper()
            if ((self.trans[i][8] > -1) and (self.trans[i][14]>0)): nm2 = self.pb_id[self.trans[i][8]].name.upper()
            if ((self.trans[i][9] > -1) and (self.trans[i][15]>0)): nm3 = self.pb_id[self.trans[i][9]].name.upper()
            if ((self.trans[i][10] > -1) and (self.trans[i][16]>0)): nm4 = self.pb_id[self.trans[i][10]].name.upper()
            if ((self.trans[i][11] > -1) and (self.trans[i][17]>0)): nm5 = self.pb_id[self.trans[i][11]].name.upper()
            ########################################
            ##  it must be a transaction of user  ##
            ########################################
            if ((self.trans[i][5] == self.user) or (self.trans[i][6] == self.user)):
                #######################################################################################
                ##  create date range, amount range, amount (part), string description, string name  ##
                #######################################################################################
                if (self.root.get_screen('Cashbook6').ids.check1.active):
                    check1="1"
                else:
                    check1="0"
                if (self.root.get_screen('Cashbook6').ids.check2.active):
                    check2="1"
                else:
                    check2="0"
                date1 = self.root.get_screen('Cashbook6').ids.date1.text
                date2 = self.root.get_screen('Cashbook6').ids.date2.text
                if (self.root.get_screen('Cashbook6').ids.amt2.text == "∞ U"):
                    sss = "999999999 U"
                else:
                    sss = self.root.get_screen('Cashbook6').ids.amt2.text
                t1 = int(self.root.get_screen('Cashbook6').ids.amt1.text[:-2])
                t2 = int(sss[:-2])
                name1 = self.root.get_screen('Cashbook6').ids.name1.text.upper()
                descr = self.root.get_screen('Cashbook6').ids.descr.text.upper()
                fits = True
                dd1=int(date1[0:2])
                mm1=int(date1[3:5])
                yy1=int(date1[6:])
                dd2=int(date2[0:2])
                mm2=int(date2[3:5])
                yy2=int(date2[6:])
                dd = int(self.trans[i][0])
                mm = int(self.trans[i][1])
                yy = int(self.trans[i][2])
                if ((check1 == "1") and (check2 == "1")):
                    if (yy<yy1): fits = False
                    if ((yy == yy1) and (mm<mm1)): fits = False
                    if ((yy == yy1) and (mm == mm1) and (dd<dd1)): fits = False
                    if (yy>yy2): fits = False
                    if ((yy == yy2) and (mm>mm2)): fits = False
                    if ((yy == yy2) and (mm == mm2) and (dd>dd2)): fits = False
                if ((check1 == "1") and (check2 == "0")):
                    if ((yy != yy1) or (mm != mm1) or (dd != dd1)): fits = False
                if ((check1 == "0") and (check2 == "1")):
                    if ((yy != yy2) or (mm != mm2) or (dd != dd2)): fits = False
                if ((amount<t1) or (amount>t2)): fits = False
                if (name1 != ""):
                    pos0 = -1
                    pos1 = -1
                    pos2 = -1
                    pos3 = -1
                    pos4 = -1
                    pos5 = -1
                    if (nm0 != ""): pos0 = nm0.find(name1)
                    if (nm1 != ""): pos1 = nm1.find(name1)
                    if (nm2 != ""): pos2 = nm2.find(name1)
                    if (nm3 != ""): pos3 = nm3.find(name1)
                    if (nm4 != ""): pos4 = nm4.find(name1)
                    if (nm5 != ""): pos5 = nm5.find(name1)
                    if ((pos0 == -1) and (pos1 == -1) and (pos2 == -1) and (pos3 == -1) and (pos4 == -1) and (pos5 == -1)): fits = False
                if (descr != ""):
                    pos0 = -1
                    if (description != ""):
                        pos0 = description.find(descr)
                    if (pos0 == -1): fits = False
            ######################################
            ##  max max_search_results results  ##
            ######################################
            fits = fits and (widgetcounter < (self.max_search_results-1))
            if (fits):
                trans_date = datetime(self.trans[i][2], self.trans[i][1], self.trans[i][0], self.trans[i][3])
                widget = TransactionEntryWidget()
                widget.id = str(i)
                widget.theme_text_color = "Custom"

                amt = round((self.trans[i][12] + self.trans[i][13] + self.trans[i][14] + self.trans[i][
                    15]+self.trans[i][16] + self.trans[i][17]), 2)
                if self.trans[i][6] == self.user:  # user = payer
                    widget.text_color = (1, 0, 0, 1)  # red
                    amt = -amt
                else:  # user = receiver
                    widget.text_color = (1, 1, 1, 1)  # white
                amt0 = amt  # past value
                shc = self.trans[i][4]
                # because the current value is calculated each time again,  ##
                # it doesn't matter that trans[] foes backward in time      ##
                for j in range(shc, self.coinscreated_past_value): # shc to now
                    amt = amt*0.99995
                widget.text = "{:.2f}".format(amt)+" U (past value: {:.2f}".format(amt0)+" U)"
                widget.secondary_text_color = (1, 1, 1, 1)  # white
                if self.trans[i][6] == self.user:  # user = payer
                    _action = "Paid"
                    _identity: Identity = self.pb_id[self.trans[i][5]]
                else:
                    _action = "Received"
                    _identity: Identity = self.pb_id[self.trans[i][6]]

                widget.secondary_text = f"{_action} on: {trans_date.strftime('%x')}"
                widget.tertiary_text = f"{_identity.name}"

                widget.ids.image_container.add_widget(create_widget_image(_identity))
                ids.cashbook_entries_container.add_widget(widget)

        self.root.current = "Cashbook7"
        self.root.transition.direction = "left"

    def go_cashbook6_r(self):
        self.from27 = 2
        self.root.current = "Cashbook6"
        self.root.transition.direction = "right"

    def go_userdata(self):
        self.root.current = "User_Data"
        self.root.transition.direction = "left"

    def go_userdata_r(self):
        """ Check if name needs to be changed on Name button """
        _user_name = self.root.get_screen('Name').ids.namefield.text
        self.root.current = "User_Data"
        self.root.transition.direction = "right"

    def go_settings(self):
        self.root.current = "Settings"
        self.root.transition.direction = "left"

    def go_settings_r(self):
        self.root.current = "Settings"
        self.root.transition.direction = "right"

    def go_about(self):
        self.root.current = "About"
        self.root.transition.direction = "left"

    def get_selected_color(self):
        return color_list[self.color_selected.lower()]

    def set_selected_color(self):
        pass

    def set_features_r(self):
        screen = "User_Data"
        self.root.get_screen(screen).ids.specialfeatures.text = self.root.get_screen(
            'Special_Features').ids.featuresfield.text
        self.root.current = screen
        self.root.transition.direction = "right"

    def goto_website(self):
        webbrowser.open("https://1coinh.com/")

    def goto_gitlab(self):
        webbrowser.open("https://gitlab.com/1coinh/onecoinh_python")

    def i18n(self, item: str):
        return self._(item)

    def _(self, item: str):
        try:
            return self.translation["i18n"][item][self.language_selected]
        except:
            return f"{item}-untranslated"

    def _set_color(self, screen: str, color: str):
        self.root.get_screen(screen).ids.colorbar.title = f"{self._('color')}: {self._(color)}"

    def set_color(self, pressed, list_id):
        self.color_selected = list_id
        self.theme_cls.primary_palette = self.color_selected
        self.theme_cls.accent_palette = self.color_selected
        self.color_selected = self.color_selected.lower()
        self.theme_text_color = "Custom"
        self.text_color = color_list[self.color_selected]
        actual_color = color_list[self.color_selected]

        self.root.get_screen('Select_Color').ids.colorbar.title = "Color: " + list_id
        self._set_color("Select_Color", self.color_selected)
        self.root.get_screen("Settings").set_color(actual_color)
        self.root.get_screen("Name").set_color(actual_color)
        self.root.get_screen("Select_Language").set_color(actual_color)

        self.root.get_screen('Home').ids.homebar.icon_color = actual_color

        self.root.get_screen('Pay').ids.label1.text_color = actual_color
        self.root.get_screen('Pay').ids.export.md_bg_color = actual_color
        self.root.get_screen('Pay').ids.paybar.icon_color = actual_color

        self.root.get_screen('Pay2').ids.label1.text_color = actual_color
        self.root.get_screen('Pay2').ids.loaddata.md_bg_color = actual_color
        self.root.get_screen('Pay2').ids.pay2bar.icon_color = actual_color

        self.root.get_screen('Pay3').ids.accept.md_bg_color = actual_color
        self.root.get_screen('Pay3').ids.label1.text_color = actual_color
        self.root.get_screen('Pay3').ids.label2.text_color = actual_color
        self.root.get_screen('Pay3').ids.label10.text_color = actual_color
        self.root.get_screen('Pay3').ids.label11.text_color = actual_color
        self.root.get_screen('Pay3').ids.label12.text_color = actual_color
        self.root.get_screen('Pay3').ids.label30.text_color = actual_color
        self.root.get_screen('Pay3').ids.label31.text_color = actual_color
        self.root.get_screen('Pay3').ids.label32.text_color = actual_color
        self.root.get_screen('Pay3').ids.label40.text_color = actual_color
        self.root.get_screen('Pay3').ids.label41.text_color = actual_color
        self.root.get_screen('Pay3').ids.label42.text_color = actual_color
        self.root.get_screen('Pay3').ids.label50.text_color = actual_color
        self.root.get_screen('Pay3').ids.label51.text_color = actual_color
        self.root.get_screen('Pay3').ids.label52.text_color = actual_color
        self.root.get_screen('Pay3').ids.label60.text_color = actual_color
        self.root.get_screen('Pay3').ids.label61.text_color = actual_color
        self.root.get_screen('Pay3').ids.label62.text_color = actual_color
        self.root.get_screen('Pay3').ids.label70.text_color = actual_color
        self.root.get_screen('Pay3').ids.label71.text_color = actual_color
        self.root.get_screen('Pay3').ids.label72.text_color = actual_color
        self.root.get_screen('Pay3').ids.label90.text_color = actual_color
        self.root.get_screen('Pay3').ids.label91.text_color = actual_color
        self.root.get_screen('Pay3').ids.label92.text_color = actual_color
        self.root.get_screen('Pay3').ids.pay3bar.icon_color = actual_color

        self.root.get_screen('Receive').ids.label1.text_color = actual_color
        self.root.get_screen('Receive').ids.nextone.md_bg_color = actual_color
        self.root.get_screen('Receive').ids.receivebar.icon_color = actual_color

        self.root.get_screen('Receive2').ids.description.theme_text_color = "Custom"
        self.root.get_screen('Receive2').ids.description.text_color = actual_color
        self.root.get_screen('Receive2').ids.description.text_color_normal = actual_color
        self.root.get_screen('Receive2').ids.description.text_color_focus = actual_color
        self.root.get_screen('Receive2').ids.description.line_color_normal = actual_color
        self.root.get_screen('Receive2').ids.description.line_color_focus = (
        1, 193 / 255, 7 / 255, 1)  # BUG when actual_color is used !!!!
        self.root.get_screen('Receive2').ids.description.icon_right_color_normal = (0.7, 0.7, 0.7, 1)
        self.root.get_screen('Receive2').ids.description.icon_right_color_focus = actual_color
        self.root.get_screen('Receive2').ids.description.hint_text_color_focus = actual_color
        self.root.get_screen('Receive2').ids.amount.theme_text_color = "Custom"
        self.root.get_screen('Receive2').ids.amount.text_color = actual_color
        self.root.get_screen('Receive2').ids.amount.text_color_normal = actual_color
        self.root.get_screen('Receive2').ids.amount.text_color_focus = actual_color
        self.root.get_screen('Receive2').ids.amount.line_color_normal = actual_color
        self.root.get_screen('Receive2').ids.amount.line_color_focus = (
        1, 193 / 255, 7 / 255, 1)  # to behave like BUG above
        self.root.get_screen('Receive2').ids.amount.icon_right_color_normal = (0.7, 0.7, 0.7, 1)
        self.root.get_screen('Receive2').ids.amount.icon_right_color_focus = actual_color
        self.root.get_screen('Receive2').ids.amount.hint_text_color_focus = actual_color
        self.root.get_screen('Receive2').ids.label2.text_color = actual_color
        self.root.get_screen('Receive2').ids.slider.thumb_color_active = actual_color
        self.root.get_screen('Receive2').ids.slider.thumb_color_inactive = actual_color
        self.root.get_screen('Receive2').ids.label10.text_color = actual_color
        self.root.get_screen('Receive2').ids.label11.text_color = actual_color
        self.root.get_screen('Receive2').ids.label12.text_color = actual_color
        self.root.get_screen('Receive2').ids.label20.text_color = actual_color
        self.root.get_screen('Receive2').ids.label21.text_color = actual_color
        self.root.get_screen('Receive2').ids.label22.text_color = actual_color
        self.root.get_screen('Receive2').ids.label30.text_color = actual_color
        self.root.get_screen('Receive2').ids.label31.text_color = actual_color
        self.root.get_screen('Receive2').ids.label32.text_color = actual_color
        self.root.get_screen('Receive2').ids.label40.text_color = actual_color
        self.root.get_screen('Receive2').ids.label41.text_color = actual_color
        self.root.get_screen('Receive2').ids.label42.text_color = actual_color
        self.root.get_screen('Receive2').ids.label50.text_color = actual_color
        self.root.get_screen('Receive2').ids.label51.text_color = actual_color
        self.root.get_screen('Receive2').ids.label52.text_color = actual_color
        self.root.get_screen('Receive2').ids.label60.text_color = actual_color
        self.root.get_screen('Receive2').ids.label61.text_color = actual_color
        self.root.get_screen('Receive2').ids.label62.text_color = actual_color
        self.root.get_screen('Receive2').ids.label70.text_color = actual_color
        self.root.get_screen('Receive2').ids.label71.text_color = actual_color
        self.root.get_screen('Receive2').ids.label72.text_color = actual_color
        self.root.get_screen('Receive2').ids.label90.text_color = actual_color
        self.root.get_screen('Receive2').ids.label91.text_color = actual_color
        self.root.get_screen('Receive2').ids.label92.text_color = actual_color
        self.root.get_screen('Receive2').ids.receive2bar.icon_color = actual_color

        self.root.get_screen('Receive3').ids.label1.text_color = actual_color
        self.root.get_screen('Receive3').ids.export.md_bg_color = actual_color
        self.root.get_screen('Receive3').ids.receive3bar.icon_color = actual_color

        self.root.get_screen('Receive4').ids.label1.text_color = actual_color
        self.root.get_screen('Receive4').ids.originator1.md_bg_color = actual_color
        self.root.get_screen('Receive4').ids.originator2.md_bg_color = actual_color
        self.root.get_screen('Receive4').ids.originator3.md_bg_color = actual_color
        self.root.get_screen('Receive4').ids.originator4.md_bg_color = actual_color
        self.root.get_screen('Receive4').ids.originator5.md_bg_color = actual_color
        self.root.get_screen('Receive4').ids.status.text_color = actual_color
        self.root.get_screen('Receive4').ids.receive4bar.icon_color = actual_color

        self.root.get_screen('Receive5').ids.amount.theme_text_color = "Custom"
        self.root.get_screen('Receive5').ids.amount.text_color = actual_color
        self.root.get_screen('Receive5').ids.amount.text_color_normal = actual_color
        self.root.get_screen('Receive5').ids.amount.text_color_focus = actual_color
        self.root.get_screen('Receive5').ids.amount.line_color_normal = actual_color
        self.root.get_screen('Receive5').ids.amount.line_color_focus = (
        1, 193 / 255, 7 / 255, 1)  # to behave like BUG above
        self.root.get_screen('Receive5').ids.amount.icon_right_color_normal = (0.7, 0.7, 0.7, 1)
        self.root.get_screen('Receive5').ids.amount.icon_right_color_focus = actual_color
        self.root.get_screen('Receive5').ids.amount.hint_text_color_focus = actual_color
        self.root.get_screen('Receive5').ids.label1.text_color = actual_color
        self.root.get_screen('Receive5').ids.check1.color_active = actual_color
        self.root.get_screen('Receive5').ids.label2.text_color = actual_color
        self.root.get_screen('Receive5').ids.check2.color_active = actual_color
        self.root.get_screen('Receive5').ids.label3.text_color = actual_color
        self.root.get_screen('Receive5').ids.check3.color_active = actual_color
        self.root.get_screen('Receive5').ids.receive5bar.icon_color = actual_color

        self.root.get_screen('Receive6').ids.export.md_bg_color = actual_color
        self.root.get_screen('Receive6').ids.receive6bar.icon_color = actual_color

        self.root.get_screen('Cashbook').ids.currenthour.text_color = actual_color
        self.root.get_screen('Cashbook').ids.cashbookbar.icon_color = actual_color

        self.root.get_screen('Cashbook2').ids.cashbook2bar.icon_color = actual_color

        self.root.get_screen('Cashbook3').ids.label1.text_color = actual_color
        self.root.get_screen('Cashbook3').ids.cashbookbar.icon_color = actual_color

        self.root.get_screen('Cashbook4').ids.cashbook4bar.icon_color = actual_color

        self.root.get_screen('Cashbook5').ids.label1.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.label3.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.label5.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.label7.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.label9.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.label11.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.label13.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.label15.text_color = actual_color
        self.root.get_screen('Cashbook5').ids.fraud.md_bg_color = actual_color
        self.root.get_screen('Cashbook5').ids.cashbookbar.icon_color = actual_color

        self.root.get_screen('Cashbook6').ids.label01.text_color = actual_color
        self.root.get_screen('Cashbook6').ids.label11.text_color = actual_color
        self.root.get_screen('Cashbook6').ids.label21.text_color = actual_color
        self.root.get_screen('Cashbook6').ids.label31.text_color = actual_color
        self.root.get_screen('Cashbook6').ids.check1.color_active = actual_color
        self.root.get_screen('Cashbook6').ids.date1.md_bg_color = actual_color
        self.root.get_screen('Cashbook6').ids.check2.color_active = actual_color
        self.root.get_screen('Cashbook6').ids.date2.md_bg_color = actual_color
        self.root.get_screen('Cashbook6').ids.amt1.md_bg_color = actual_color
        self.root.get_screen('Cashbook6').ids.amt2.md_bg_color = actual_color
        self.root.get_screen('Cashbook6').ids.name1.text_color = actual_color
        self.root.get_screen('Cashbook6').ids.name1.text_color_normal = actual_color
        self.root.get_screen('Cashbook6').ids.name1.text_color_focus = actual_color
        self.root.get_screen('Cashbook6').ids.name1.line_color_normal = actual_color
        self.root.get_screen('Cashbook6').ids.name1.line_color_focus = (1, 193 / 255, 7 / 255, 1) # to behave like BUG above
        self.root.get_screen('Cashbook6').ids.name1.icon_right_color_normal = (0.7,0.7,0.7,1)
        self.root.get_screen('Cashbook6').ids.name1.icon_right_color_focus = actual_color
        self.root.get_screen('Cashbook6').ids.name1.hint_text_color_focus = actual_color
        self.root.get_screen('Cashbook6').ids.descr.text_color = actual_color
        self.root.get_screen('Cashbook6').ids.descr.text_color_normal = actual_color
        self.root.get_screen('Cashbook6').ids.descr.text_color_focus = actual_color
        self.root.get_screen('Cashbook6').ids.descr.line_color_normal = actual_color
        self.root.get_screen('Cashbook6').ids.descr.line_color_focus = (1, 193 / 255, 7 / 255, 1) # to behave like BUG above
        self.root.get_screen('Cashbook6').ids.descr.icon_right_color_normal = (0.7,0.7,0.7,1)
        self.root.get_screen('Cashbook6').ids.descr.icon_right_color_focus = actual_color
        self.root.get_screen('Cashbook6').ids.descr.hint_text_color_focus = actual_color
        self.root.get_screen('Cashbook6').ids.cashbookbar.icon_color = actual_color

        self.root.get_screen('Cashbook7').ids.cashbook7bar.icon_color = actual_color

        self.root.get_screen('About').ids.aboutbar.icon_color = actual_color
        self.root.get_screen('About').ids.my_label.text_color = actual_color
        self.root.get_screen('About').ids.my_label1.text_color = actual_color
        self.root.get_screen('About').ids.my_label3.text_color = actual_color
        self.root.get_screen('About').ids.my_label4.text_color = actual_color
        self.root.get_screen('About').ids.website.md_bg_color = actual_color
        self.root.get_screen('About').ids.gitlab.md_bg_color = actual_color

        self.root.get_screen('Select_Color').ids.colorbar.icon_color = actual_color
        self.root.get_screen('Backup').ids.backupbar.icon_color = actual_color
        self.root.get_screen('Restore').ids.restorebar.icon_color = actual_color
        self.root.get_screen('Clear_Data').ids.clear_databar.icon_color = actual_color

        self.root.get_screen('Height').ids.heightbar.icon_color = actual_color
        self.root.get_screen('Height').ids.meters.theme_text_color = "Custom"
        self.root.get_screen('Height').ids.meters.text_color = actual_color
        self.root.get_screen('Height').ids.meters.line_color_normal = actual_color
        self.root.get_screen('Height').ids.meters.line_color_focus = actual_color
        self.root.get_screen('Height').ids.meters.hint_text_color_normal = actual_color
        self.root.get_screen('Height').ids.meters.hint_text_color_focus = actual_color
        self.root.get_screen('Height').ids.metersfeetlabel.theme_text_color = "Custom"
        self.root.get_screen('Height').ids.metersfeetlabel.text_color = actual_color
        self.root.get_screen('Height').ids.meterswitch.thumb_color_active = actual_color
        self.root.get_screen('Height').ids.meterswitch.thumb_color_inactive = actual_color
        self.root.get_screen('Height').ids.feet.theme_text_color = "Custom"
        self.root.get_screen('Height').ids.feet.text_color = actual_color
        self.root.get_screen('Height').ids.feet.line_color_normal = actual_color
        self.root.get_screen('Height').ids.feet.line_color_focus = actual_color
        self.root.get_screen('Height').ids.feet.hint_text_color_normal = actual_color
        self.root.get_screen('Height').ids.feet.hint_text_color_focus = actual_color
        self.root.get_screen('Height').ids.inches.theme_text_color = "Custom"
        self.root.get_screen('Height').ids.inches.text_color = actual_color
        self.root.get_screen('Height').ids.inches.line_color_normal = actual_color
        self.root.get_screen('Height').ids.inches.line_color_focus = actual_color
        self.root.get_screen('Height').ids.inches.hint_text_color_normal = actual_color
        self.root.get_screen('Height').ids.inches.hint_text_color_focus = actual_color

        self.root.get_screen('Special_Features').ids.featuresbar.icon_color = actual_color
        self.root.get_screen('Special_Features').ids.featuresfield.theme_text_color = "Custom"
        self.root.get_screen('Special_Features').ids.featuresfield.text_color = actual_color
        self.root.get_screen('Special_Features').ids.featuresfield.line_color_normal = actual_color
        self.root.get_screen('Special_Features').ids.featuresfield.line_color_focus = actual_color
        self.root.get_screen('Special_Features').ids.featuresfield.hint_text_color_normal = actual_color
        self.root.get_screen('Special_Features').ids.featuresfield.hint_text_color_focus = actual_color
        self.root.get_screen('Special_Features').ids.label2.text_color = actual_color

        self.root.get_screen('Set_Image').ids.set_imagebar.icon_color = actual_color

    def set_language(self, pressed, lang_selected):
        """ Set language """
        self.language_selected = lang_selected
        self.root.get_screen('Pay').ids.paytitle.title = self._('pay')
        self.root.get_screen('Pay').ids.label1.text = self._('let_receiver_of_your_coins')
        self.root.get_screen('Pay').ids.export.text = self._('or_export_payer_data')

        self.root.get_screen('Pay2').ids.pay2title.title = self._('pay')
        self.root.get_screen('Pay2').ids.label1.text = self._('scan_the_qrcode_of_the_receiver_of_the_coins')
        self.root.get_screen('Pay2').ids.loaddata.text = self._('or_load_the_receiver_data')

        self.root.get_screen('Pay3').ids.pay3title.title = self._('pay')
        self.root.get_screen('Pay3').ids.accept.text = self._('accept_and_process')

        self.root.get_screen('Receive').ids.receivetitle.title = self._('receive')
        self.root.get_screen('Receive').ids.label1.text = self._('scan_the_qrcode_of_the_payer_of_the_coins')
        self.root.get_screen('Receive').ids.nextone.text = self._('or_load_the_payer_data')

        self.root.get_screen('Receive2').ids.receivetitle.title = self._('receive_list')
        self.root.get_screen('Receive2').ids.description.hint_text = self._('description_of_transaction')
        self.root.get_screen('Receive2').ids.amount.hint_text = self._('amount_you_want_to_receive')
        self.root.get_screen('Receive2').ids.label91.text = self._('total')

        self.root.get_screen('Receive3').ids.receive3title.title = self._('receive')
        self.root.get_screen('Receive3').ids.label1.text = self._('let_payer_scan_this_qr_code')
        self.root.get_screen('Receive3').ids.export.text = self._('or_export_receiver_data')

        self.root.get_screen('Receive4').ids.receive4title.title = self._('receive_list')
        self.root.get_screen('Receive4').ids.label1.text = self._('select_person_you_want_to_change')

        self.root.get_screen('Receive5').ids.receive5title.title = self._('receive_list')
        self.root.get_screen('Receive5').ids.amount.hint_text = self._('amount')
        self.root.get_screen('Receive5').ids.label1.text = self._('security')
        self.root.get_screen('Receive5').ids.label2.text = self._('name')
        self.root.get_screen('Receive5').ids.label3.text = self._('amount')

        self.root.get_screen('Receive6').ids.receive6title.title = self._('receive')
        self.root.get_screen('Receive6').ids.export.text = self._('payer_accepts_your_proposal')

        self.root.get_screen('Cashbook').ids.cashbooktitle.title = self._('cashbook')
        self.root.get_screen('Cashbook').ids.label11.text = " " + self._('self_created')
        self.root.get_screen('Cashbook').ids.label20.text = self._('past_value')
        self.root.get_screen('Cashbook').ids.label30.text = self._('present_value')
        self.root.get_screen('Cashbook').ids.label41.text = " " + self._('self_created')
        self.root.get_screen('Cashbook').ids.label51.text = " " + self._('spent')
        self.root.get_screen('Cashbook').ids.label71.text = " " + self._('from_others')
        self.root.get_screen('Cashbook').ids.label81.text = " " + self._('spent')
        self.root.get_screen('Cashbook').ids.label91.text = " " + self._('available')

        self.root.get_screen('Cashbook2').ids.cashbooktitle.title = self._('cashbook')

        self.root.get_screen('Cashbook3').ids.cashbooktitle.title = self._('cashbook')

        self.root.get_screen('Cashbook4').ids.cashbooktitle.title = self._('cashbook')

        self.root.get_screen('Cashbook5').ids.cashbooktitle.title = self._('cashbook')
        self.root.get_screen('Cashbook5').ids.label1.text = " "+self._('name')
        self.root.get_screen('Cashbook5').ids.label3.text = " "+self._('birthday')
        self.root.get_screen('Cashbook5').ids.label5.text = " "+self._('gender')
        self.root.get_screen('Cashbook5').ids.label7.text = " "+self._('height')
        self.root.get_screen('Cashbook5').ids.label9.text = " "+self._('hair')
        self.root.get_screen('Cashbook5').ids.label11.text = " "+self._('left_eye')
        self.root.get_screen('Cashbook5').ids.label13.text = " "+self._('right_eye')
        self.root.get_screen('Cashbook5').ids.label15.text = " "+self._('features')
        self.root.get_screen('Cashbook5').ids.fraud.text = " "+self._('mark_as_fraud')

        self.root.get_screen('Cashbook6').ids.cashbooktitle.title = self._('cashbook')
        self.root.get_screen('Cashbook6').ids.label01.text = " "+self._('from_on')
        self.root.get_screen('Cashbook6').ids.label11.text = " "+self._('until')
        self.root.get_screen('Cashbook6').ids.label21.text = " "+self._('from')
        self.root.get_screen('Cashbook6').ids.label31.text = " "+self._('to')
        self.root.get_screen('Cashbook6').ids.name1.hint_text = " "+self._('name')
        self.root.get_screen('Cashbook6').ids.descr.hint_text = " "+self._('description')

        self.root.get_screen('About').ids.abouttitle.title = self._('about')
        self.root.get_screen('About').ids.my_label1.text = self._('ethical_money')
        self.root.get_screen('About').ids.my_label3.text = self._('open_source_software')
        self.root.get_screen('About').ids.my_label4.text = self._('version') + " 2.01.01"

        self.root.get_screen('Settings').ids.userdata.text = self._('userdata')
        self.root.get_screen('Settings').ids.language.text = self._('language')
        self.root.get_screen('Settings').ids.screencolor.text = self._('screen_color')
        self.root.get_screen('Settings').ids.settingstitle.title = self._('settings')
        self.root.get_screen('Settings').ids.backup.text = self._('backup')
        self.root.get_screen('Settings').ids.restore.text = self._('restore')
        self.root.get_screen('Settings').ids.clear_data.text = self._('clear_data')
        self.root.get_screen('Backup').ids.backup.title = self._('backup_local_data')
        self.root.get_screen('Restore').ids.restore.title = self._('restore_local_data')
        self.root.get_screen('Clear_Data').ids.clear_data.title = self._('clear_local_data')
        self.root.get_screen('Select_Language').ids.languagetitle.title = self._('select_app_language')
        self.root.get_screen('Select_Language').ids.languagebar.title = self._("language")
        self.root.get_screen('Select_Color').ids.selectcolor.title = self._('select_screen_color')
        for _color in color_list:
            for _id in self.root.get_screen('Select_Color').ids:
                if _id == _color:
                    self.root.get_screen('Select_Color').ids[_color].text = self._(_color)

        self.root.get_screen('User_Data').ids.userdatatitle.title = self._("userdata")




        ###########################################################
        ## here we are setting the buttons of the User_Data page ##
        ## using all the user variables                          ##
        ###########################################################
        _selected_screen = self.root.get_screen('User_Data')
        # Check if name needs to be changed on Name button
        OneCoinHApp.user_name = self.root.get_screen('Name').ids.namefield.text
        if OneCoinHApp.user_name == "":
            _selected_screen.ids.fullname.text = self._("name")
        else:
            _selected_screen.ids.fullname.text = OneCoinHApp.user_name
        # Check if birthday needs to be changed on Name button  ##
        if self.user_birthday_day == 0:
            s_birthday = self._('birthday')
        else:
            s_day = str(self.user_birthday_day)
            if self.user_birthday_day < 10:
                s_day = "0" + s_day
            s_month = str(self.user_birthday_month)
            if self.user_birthday_month < 10:
                s_month = "0" + s_month
            s_year = str(self.user_birthday_year)
            s_birthday = s_day + "-" + s_month + "-" + s_year
        _selected_screen.ids.birthday.text = s_birthday
        # possibly change the gender on the Gender button  ##
        s_gender = self._(self.user_gender)  # this can be the word "Gender" or the actual Gender
        if (s_gender == self._('choose')):
            _selected_screen.ids.gender.text = self._('gender')
        else:
            _selected_screen.ids.gender.text = s_gender

        user_height = self._('height')
        if (self.user_height_switch):
            if (self.user_feet > 0):
                user_height = str(self.user_feet) + 'ft,' + str(self.user_inches) + 'in'
            else:
                user_height = self._('height')
        else:
            if (self.user_meters > 0):
                user_height = str(self.user_meters) + 'm'
            else:
                user_heighth = self._('height')
        _selected_screen.ids.height.text = self._('height')

        s_hair = self._(self.user_hair)  # this can be the word "Hair" or the actual Haircolor
        if (s_hair == self._('choose')):
            _selected_screen.ids.hair.text = self._('hair')
        else:
            _selected_screen.ids.hair.text = s_hair

        s_left_eye = self._(self.user_left_eye)  # this can be the word "Left Eye" or the actual left eye color
        if (s_left_eye == self._('choose')):
            _selected_screen.ids.left_eye.text = self._('left_eye')
        else:
            _selected_screen.ids.left_eye.text = s_left_eye

        s_right_eye = self._(self.user_right_eye)  # this can be the word "Right Eye" or the actual right eye color
        if (s_right_eye == self._('choose')):
            _selected_screen.ids.right_eye.text = self._('right_eye')
        else:
            _selected_screen.ids.right_eye.text = s_right_eye

        _selected_screen.ids.specialfeatures.text = self._('special_features')
        _selected_screen.ids.set_image.text = self._('set_image')
        #######################################################################################
        ## then we will set the title and bottombar of the screens connected to the User_Data   ##
        ## In time all these screens should be displayed by dialogboxes if we can figure out ##
        ## how he dialogboxes can be changed dynamically using their .kv layout              ##
        #######################################################################################
        self.root.get_screen('Name').ids.fullname.title = self._('name')
        self.root.get_screen('Name').ids.namefield.hint_text = self._('enter_your_full_name')
        self.root.get_screen('Name').ids.label1.text = self._('use_a_minimum_2_of_words')
        self.root.get_screen('Name').ids.label2.text = self._('use_a_maximum_of_32_characters')
        self.root.get_screen('Name').ids.namebar.title = self._('choose')

        self.root.get_screen('Height').ids.height.title = self._('height')
        self.root.get_screen('Height').ids.metersfeetlabel.text = self._('meters') + " < - > " + self._('feet') + "/" + self._('inches')
        self.root.get_screen('Height').ids.meters.hint_text = self._('enter_your_height') + " ("+self._('meters')+")"
        self.root.get_screen('Height').ids.feet.hint_text = self._('enter_your_height') + " ("+self._('feet')+")"
        self.root.get_screen('Height').ids.inches.hint_text = self._('remaining_height') + " ("+self._('inches')+")"
        user_height = self._('height')
        if self.user_height_switch:
            if self.user_feet > 0:
                user_height = str(self.user_feet)+'ft,'+ str(self.user_inches)+'in'
            else:
                user_height = self._('height')
        else:
            if self.user_meters > 0:
                user_height = str(self.user_meters)+'m'
            else:
                user_height = self._('height')
        self.root.get_screen('User_Data').ids.height.text = user_height
        self.root.get_screen('Height').ids.heightbar.title = user_height

        self.root.get_screen('Special_Features').ids.specialfeatures.title = self._('special_features')
        self.root.get_screen('Special_Features').ids.featuresfield.hint_text = self._('enter_your_features')
        self.root.get_screen('Special_Features').ids.label2.text = self._('use_a_maximum_of_32_characters')

        self.root.get_screen('Set_Image').ids.set_image.title = self._('set_image')



    def on_save(self, instance, value, date_range):
        """ Save the birthday """
        picker_day = int(str(value)[8:])
        picker_month = int(str(value)[5:7])
        picker_year = int(str(value)[0:4])
        self.user_birthday_day = picker_day
        self.user_birthday_month = picker_month
        self.user_birthday_year = picker_year
        s_day = str(picker_day)
        if picker_day < 10:
            s_day = "0" + s_day
        s_month = str(picker_month)
        if picker_month < 10:
            s_month = "0" + s_month
        s_year = str(picker_year)
        s_date = s_day + "-" + s_month + "-" + s_year
        self.root.get_screen('User_Data').ids.birthday.text = s_date

    def on_cancel(self, instance, value):
        pass

    def set_birthday(self, pressed, birthday_selected):
        """ Set the default date for the DatePicker """
        if self.user_birthday_day == 0:
            picker_day = 1
        else:
            picker_day = self.user_birthday_day
        if self.user_birthday_month == 0:
            picker_month = 1
        else:
            picker_month = self.user_birthday_month
        if self.user_birthday_year == 0:
            picker_year = 2022
        else:
            picker_year = self.user_birthday_year

        # Set the DatePicker variables, functions and start it
        date_dialog = MDDatePicker(
            title=self._('select_birthday').upper(),
            title_input=self._('type_birthday').upper(),
            helper_text=self._('wrong_date'),
            max_date=datetime.today(), day=picker_day, month=picker_month, year=picker_year)
        date_dialog.bind(on_save=self.on_save, on_cancel=self.on_cancel)
        date_dialog.open()


    def set_gender(self, pressed, gender_selected):
        """ Set the gender for the User_Data Page """
        OneCoinHApp.user_gender = gender_selected
        if gender_selected == "male":
            self.user_gender = "male"
        elif gender_selected == "female":
            self.user_gender = "female"
        elif gender_selected == "other":
            self.user_gender = "other"
        else:
            self.user_gender = "gender"
        s_gender = self._(self.user_gender)
        self.root.get_screen('User_Data').ids.gender.text = s_gender
        #self.root.get_screen('Gender').ids.gendertitle.title = self._('gender')
        self.root.get_screen('Gender').ids.genderbar.title = s_gender


    def dropdown3(self):
        self.menu_items = [
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('male'),
                "on_release": lambda x=self.i18n('male'): self.test3("male")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('female'),
                "on_release": lambda x=self.i18n('female'): self.test3("female")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('other'),
                "on_release": lambda x=self.i18n('other'): self.test3("other")
            }
        ]
        self.menu = MDDropdownMenu(
            caller=self.root.get_screen('User_Data').ids.gender,
            items=self.menu_items,
            width_mult=2
        )
        self.menu.open()


    def test3(self, gender_selected):
        self.menu.clear_widgets()
        OneCoinHApp.user_gender = gender_selected
        if gender_selected == "male":
            self.user_gender = "male"
        elif gender_selected == "female":
            self.user_gender = "female"
        elif gender_selected == "other":
            self.user_gender = "other"
        else:
            self.user_gender = "gender"
        s_gender = self._(self.user_gender)
        self.root.get_screen('User_Data').ids.gender.text = s_gender


    def set_height(self):
        # text = self.root.ids.meters.text
        text1 = self.root.get_screen("Height").ids.meters.text
        text2 = self.root.get_screen("Height").ids.feet.text
        text3 = self.root.get_screen("Height").ids.inches.text
        self.user_height_switch = self.root.get_screen("Height").ids.meterswitch.active
        if (len(text1) < 1):
            text1 = "0"
        if (len(text2) < 1):
            text2 = "0"
        if (len(text3) < 1):
            text3 = "0"
        user_height = self._('height')
        self.user_meters = float(text1)
        self.user_feet = int(text2)
        self.user_inches = int(text3)
        if (self.user_height_switch):
            if (self.user_feet > 0):
                user_height = str(self.user_feet) + 'ft,' + str(self.user_inches) + 'in'
            else:
                user_height = self._('height')
        else:
            if (self.user_meters > 0):
                user_height = str(self.user_meters) + 'm'
            else:
                user_heighth = self._('height')
        self.root.get_screen('User_Data').ids.height.text = user_height
        #self.root.get_screen('Height').ids.heighttitle.title = self._('height')
        self.root.get_screen('Height').ids.heightbar.title = user_height


    def dropdown4(self):
        self.menu_items = [
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('white'),
                "on_release": lambda x=self.i18n('white'): self.test4("white")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('gingerred'),
                "on_release": lambda x=self.i18n('gingerred'): self.test4("gingerred")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('auburn'),
                "on_release": lambda x=self.i18n('auburn'): self.test4("auburn")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('blond'),
                "on_release": lambda x=self.i18n('blond'): self.test4("blond")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('lightchesnut'),
                "on_release": lambda x=self.i18n('lightchesnut'): self.test4("lightchesnut")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('copper'),
                "on_release": lambda x=self.i18n('copper'): self.test4("copper")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('lightblond'),
                "on_release": lambda x=self.i18n('lightblond'): self.test4("lightblond")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('chesnutbrown'),
                "on_release": lambda x=self.i18n('chesnutbrown'): self.test4("chesnutbrown")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('silver'),
                "on_release": lambda x=self.i18n('silver'): self.test4("silver")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('medblond'),
                "on_release": lambda x=self.i18n('medblond'): self.test4("medblond")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('lightbrown'),
                "on_release": lambda x=self.i18n('lightbrown'): self.test4("lightbrown")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('titan'),
                "on_release": lambda x=self.i18n('titan'): self.test4("titan")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('darkblond'),
                "on_release": lambda x=self.i18n('darkblond'): self.test4("darkblond")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('medbrown'),
                "on_release": lambda x=self.i18n('medbrown'): self.test4("medbrown")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('gray'),
                "on_release": lambda x=self.i18n('gray'): self.test4("gray")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('goldblond'),
                "on_release": lambda x=self.i18n('goldblond'): self.test4("goldblond")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('darkbrown'),
                "on_release": lambda x=self.i18n('darkbrown'): self.test4("darkbrown")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('black'),
                "on_release": lambda x=self.i18n('black'): self.test4("black")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('strawberryblond'),
                "on_release": lambda x=self.i18n('strawberryblond'): self.test4("strawberryblond")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('nohair'),
                "on_release": lambda x=self.i18n('nohair'): self.test4("nohair")
            }
        ]
        self.menu = MDDropdownMenu(
            caller=self.root.get_screen('User_Data').ids.hair,
            items=self.menu_items,
            width_mult=4
        )
        self.menu.open()


    def test4(self, hair_selected):
        self.menu.clear_widgets()
        OneCoinHApp.user_hair = hair_selected
        if hair_selected == "white":
            self.user_hair = "white"
        elif hair_selected == "gingerred":
            self.user_hair = "gingerred"
        elif hair_selected == "auburn":
            self.user_hair = "auburn"
        elif hair_selected == "blond":
            self.user_hair = "blond"
        elif hair_selected == "lightchesnut":
            self.user_hair = "lightchesnut"
        elif hair_selected == "copper":
            self.user_hair = "copper"
        elif hair_selected == "lightblond":
            self.user_hair = "lightblond"
        elif hair_selected == "chesnutbrown":
            self.user_hair = "chesnutbrown"
        elif hair_selected == "silver":
            self.user_hair = "silver"
        elif hair_selected == "medblond":
            self.user_hair = "medblond"
        elif hair_selected == "lightbrown":
            self.user_hair = "lightbrown"
        elif hair_selected == "titan":
            self.user_hair = "titan"
        elif hair_selected == "darkblond":
            self.user_hair = "darkblond"
        elif hair_selected == "medbrown":
            self.user_hair = "medbrown"
        elif hair_selected == "gray":
            self.user_hair = "gray"
        elif hair_selected == "goldblond":
            self.user_hair = "goldblond"
        elif hair_selected == "darkbrown":
            self.user_hair = "darkbrown"
        elif hair_selected == "black":
            self.user_hair = "black"
        elif hair_selected == "strawberryblond":
            self.user_hair = "strawberryblond"
        elif hair_selected == "nohair":
            self.user_hair = "nohair"
        else:
            self.user_hair = "hair"
        s_hair = self._(self.user_hair)
        self.root.get_screen('User_Data').ids.hair.text = s_hair


    def dropdown5(self):
        self.menu_items = [
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('amber'),
                "on_release": lambda x=self.i18n('amber'): self.test5("amber")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('blue'),
                "on_release": lambda x=self.i18n('blue'): self.test5("blue")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('brown'),
                "on_release": lambda x=self.i18n('brown'): self.test5("brown")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('gray'),
                "on_release": lambda x=self.i18n('gray'): self.test5("gray")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('green'),
                "on_release": lambda x=self.i18n('green'): self.test5("green")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('hazel'),
                "on_release": lambda x=self.i18n('hazel'): self.test5("hazel")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('red'),
                "on_release": lambda x=self.i18n('red'): self.test5("red")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('bluegray'),
                "on_release": lambda x=self.i18n('bluegray'): self.test5("bluegray")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('bluegreen'),
                "on_release": lambda x=self.i18n('bluegreen'): self.test5("bluegreen")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('greengray'),
                "on_release": lambda x=self.i18n('greengray'): self.test5("greengray")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('prosthesis'),
                "on_release": lambda x=self.i18n('prosthesis'): self.test5("prosthesis")
            }
        ]
        self.menu = MDDropdownMenu(
            caller=self.root.get_screen('User_Data').ids.left_eye,
            items=self.menu_items,
            width_mult=4
        )
        self.menu.open()


    def test5(self, left_eye_selected):
        self.menu.clear_widgets()
        OneCoinHApp.user_left_eye = left_eye_selected
        if left_eye_selected == "amber":
            self.user_left_eye = "amber"
        elif left_eye_selected == "blue":
            self.user_left_eye = "blue"
        elif left_eye_selected == "brown":
            self.user_left_eye = "brown"
        elif left_eye_selected == "gray":
            self.user_left_eye = "gray"
        elif left_eye_selected == "green":
            self.user_left_eye = "green"
        elif left_eye_selected == "hazel":
            self.user_left_eye = "hazel"
        elif left_eye_selected == "red":
            self.user_left_eye = "red"
        elif left_eye_selected == "bluegray":
            self.user_left_eye = "bluegray"
        elif left_eye_selected == "bluegreen":
            self.user_left_eye = "bluegreen"
        elif left_eye_selected == "greengray":
            self.user_left_eye = "greengray"
        elif left_eye_selected == "prosthesis":
            self.user_left_eye = "prosthesis"
        else:
            self.user_left_eye = "left_eye"
        s_left_eye = self._(self.user_left_eye)
        self.root.get_screen('User_Data').ids.left_eye.text = s_left_eye


    def dropdown6(self):
        self.menu_items = [
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('amber'),
                "on_release": lambda x=self.i18n('amber'): self.test6("amber")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('blue'),
                "on_release": lambda x=self.i18n('blue'): self.test6("blue")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('brown'),
                "on_release": lambda x=self.i18n('brown'): self.test6("brown")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('gray'),
                "on_release": lambda x=self.i18n('gray'): self.test6("gray")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('green'),
                "on_release": lambda x=self.i18n('green'): self.test6("green")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('hazel'),
                "on_release": lambda x=self.i18n('hazel'): self.test6("hazel")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('red'),
                "on_release": lambda x=self.i18n('red'): self.test6("red")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('bluegray'),
                "on_release": lambda x=self.i18n('bluegray'): self.test6("bluegray")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('bluegreen'),
                "on_release": lambda x=self.i18n('bluegreen'): self.test6("bluegreen")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('greengray'),
                "on_release": lambda x=self.i18n('greengray'): self.test6("greengray")
            },
            {
                "viewclass": "OneLineListItem",
                "text": self.i18n('prosthesis'),
                "on_release": lambda x=self.i18n('prosthesis'): self.test6("prosthesis")
            }
        ]
        self.menu = MDDropdownMenu(
            caller=self.root.get_screen('User_Data').ids.right_eye,
            items=self.menu_items,
            width_mult=4
        )
        self.menu.open()


    def test6(self, right_eye_selected):
        self.menu.clear_widgets()
        OneCoinHApp.user_right_eye = right_eye_selected
        if right_eye_selected == "amber":
            self.user_right_eye = "amber"
        elif right_eye_selected == "blue":
            self.user_right_eye = "blue"
        elif right_eye_selected == "brown":
            self.user_right_eye = "brown"
        elif right_eye_selected == "gray":
            self.user_right_eye = "gray"
        elif right_eye_selected == "green":
            self.user_right_eye = "green"
        elif right_eye_selected == "hazel":
            self.user_right_eye = "hazel"
        elif right_eye_selected == "red":
            self.user_right_eye = "red"
        elif right_eye_selected == "bluegray":
            self.user_right_eye = "bluegray"
        elif right_eye_selected == "bluegreen":
            self.user_right_eye = "bluegreen"
        elif right_eye_selected == "greengray":
            self.user_right_eye = "greengray"
        elif right_eye_selected == "prosthesis":
            self.user_right_eye = "prosthesis"
        else:
            self.user_right_eye = "right_eye"
        s_right_eye = self._(self.user_right_eye)
        self.root.get_screen('User_Data').ids.right_eye.text = s_right_eye


    def set_receive(self):
        ################################################
        ##  Retrieve the information from the screen  ##
        #############################################################################
        ##  Get the amount the User entered in the MDTextField                     ##
        ##  and check if its a proper float and return it formatted to the screen  ##
        #############################################################################
        data = self.root.get_screen("Receive2").ids.amount.text
        try:
            amount = float(data)
        except ValueError:
            amount = 0
        amount = round(amount, 2)
        self.root.get_screen("Receive2").ids.amount.text = "{:.2f}".format(amount)
        ###########################################################################
        ##  Get the position of the slider and put the percentage on the screen  ##
        ###########################################################################
        slider_value = int(self.root.get_screen('Receive2').ids.slider.value)
        #################################################################
        ##  Get the sort mode and store it in the 'mode' variable      ##
        ##  4 modes: 'descending', 'ascending', 'random' and 'manual'  ##
        #################################################################
        mode_icon = self.root.get_screen('Receive2').ids.amount.icon_right
        mode = "descending"   # default: mode_icon == "sort-variant:
        if (mode_icon == "sort-reverse-variant"):
            mode = "ascending"
        if (mode_icon == "shuffle-variant"):
            mode = "random"
        if (mode_icon == "fountain-pen-tip"):
            mode = "manual"
        ###########################################################################################
        ##  we start with                                    PB means Personal Blockchain        ##
        ##  pb_person = [['name', 'hash', amount],[],...]     (list from PB receiver)            ##
        ##  pb_items = number of rows in pb_list                                                 ##
        ##  the PB is used for the security indicators                                           ##
        ##  snapshot_payer = ['short name',  'short hash', amount, trans]   (list from QR Code)  ##
        ##  snapshot_person =[['short name',  'short hash', amount, trans],[..,..,..]...]        ##
        ##  snapshot_items = number of rows in snapshot list                                     ##
        # self.snapshot_items = 2 # FOR TESTING
        ###########################################################################################
        ##  We create 3 lists to set the 5 others slots     ##
        ##  - indexlist are 5 pointers to the sortedlist    ##
        ##  - amountlist are the available coins per other  ##
        ##  - otherslist is ratio'd amount per other        ##
        ######################################################
        if (self.pb_items > 0):
            ##############################################
            ##  See if payer is mutual and if it does:  ##
            ##    - change snapshot name to full name   ##
            ##    - change snapshot hash to full hash   ##
            ##    - change trans code to BIGGEST!       ##
            ##    - change mutual boolean to True       ##
            ##############################################
            for j in range(0, self.pb_items):
                if ((self.pb_person[j][0].find(self.snapshot_payer[0]) == 0) and
                        (self.pb_person[j][1].find(self.snapshot_payer[1]) == 0)):
                    self.snapshot_payer[0] = self.pb_person[j][0]
                    self.snapshot_payer[1] = self.pb_person[j][1]
                    if (self.snapshot_payer[3] < self.pb_person[j][3]):
                        self.snapshot_payer[3] = self.pb_person[j][3]
                    self.snapshot_payer[4] = True
            ##############################################
            ##  See if payer is mutual and if it does:  ##
            ##    - change snapshot name to full name   ##
            ##    - change snapshot hash to full hash   ##
            ##    - change trans code to BIGGEST!       ##
            ##    - change mutual boolean to True       ##
            ##############################################
            if (self.snapshot_items > 0):
                for i in range(0, self.snapshot_items):
                    for j in range(0, self.pb_items):
                        if ((self.pb_person[j][0].find(self.snapshot_person[i][0]) == 0) and
                                (self.pb_person[j][1].find(self.snapshot_person[i][1]) == 0)):
                            self.snapshot_person[i][0] = self.pb_person[j][0]
                            self.snapshot_person[i][1] = self.pb_person[j][1]
                            if (self.snapshot_person[i][3] < self.pb_person[j][3]):
                                self.snapshot_person[i][3] = self.pb_person[j][3]
                            self.snapshot_person[i][4] = True
        #####################################################
        ## We sort the snapshot file from small to large   ##
        ## and put the results in a new array: sortedlist  ##
        #####################################################
        self.sortedlist = sorted(self.snapshot_person, key=lambda x: x[2])  # sort on amount
        ###############################################################
        ##  Calculate the maximum available coins in the snapshot    ##
        ##  this is all the coins of payer plus the 5 others with    ##
        ##  If this is less than the amount the receiver wants,      ##
        ##  there is no combination of coins from of payer+5 others  ##
        ##  that is enough to pay the amount, which results in an    ##
        ##  error message. What the payer can maximally pay is       ##
        ##  stored in 2 variables: 'payer_coins' and 'fivelargest'   ##
        ##  in the for loop we need to consider the possibility      ##
        ##  that there are less than 5 others to use coins from      ##
        ###############################################################
        payer_coins = self.snapshot_payer[2]
        fivelargest = 0
        startitem = self.snapshot_items - 5
        enditem = self.snapshot_items
        if (startitem < 0):
            startitem = 0
        if (self.snapshot_items > 0):
            for i in range(startitem, enditem):
                fivelargest = fivelargest + self.sortedlist[i][2]
        available_coins = payer_coins + fivelargest
        ################################################################################
        ##  First set the indexlist and amountlist in the 'descending' others order   ##
        ##  This is also the fall back result is ascending or random result in lists  ##
        ##  that can not fill the total needed amount                                 ##
        ################################################################################
        self.root.get_screen('Receive2').ids.slider.opacity = 0
        self.root.get_screen('Receive2').ids.slider.disabled = True
        self.root.get_screen('Receive2').ids.label2.text = ""
        if (self.snapshot_items > 0):
            self.root.get_screen('Receive2').ids.slider.opacity = 1
            self.root.get_screen('Receive2').ids.slider.disabled = False
            self.root.get_screen('Receive2').ids.label2.text = "."
            if (mode != "manual"):
                for i in range(startitem, enditem):
                    if (self.snapshot_items > i):
                        self.indexlist[i - startitem] = self.sortedlist[i][5]
                        self.amountlist[i - startitem] = self.sortedlist[i][2]
        #########################################################################################
        ##  If descending is not sufficient to fill up the entire amount, no combination will  ##
        ##  in that case we can give an error message and skip all modes                       ##
        #########################################################################################
        if (amount > available_coins):
            ###########################################
            ##  No combination can fill the amount.  ##
            ##  We provide the error message         ##
            ###########################################
            toast(self.i18n('amount_is_too_large_for_payer'), duration=3)
            ####################################################
            ##  So the slider is set to 100% because we want  ##
            ##  to show the amount that is maximum available  ##
            ####################################################
            self.root.get_screen('Receive2').ids.slider.value = 100
            ########################################################################
            ##  Here we need to block the user to use the 'further' button,       ##
            ##  since the amount is to much to be paid for the payer              ##
            ##  self.root.get_screen("Receive2").ids.receive2bar.disabled = True  ##
            ##    this instruction doesnt work on the floating button             ##
            ########################################################################
        else:
            if (self.snapshot_items > 0):
                ###########################################################################################
                ##  there are "others" in the snapshot and with the largest(s), the payment is possible  ##
                ##  so now we need to go through the other modes                                         ##
                ###########################################################################################
                if (mode == "ascending"):
                    available_coins = 0
                    startitem = 0
                    enditem = 5
                    if (enditem > self.snapshot_items):
                        enditem = self.snapshot_items
                    total = 0
                    while (total < amount):
                        #########################################################################
                        ##  Ascending goes in 2 stages:                                        ##
                        ##  1st stage we use the 5 smallest and move slider up until it fills  ##
                        ##  2rd stage we start increasing the 5 smallest until it fills        ##
                        #########################################################################
                        fivesmallest = 0
                        for i in range(startitem, enditem):
                            fivesmallest = fivesmallest + self.sortedlist[i][2]
                        totalslider = (amount * slider_value / 100)
                        if (totalslider > payer_coins):
                            totalslider = payer_coins
                        total = totalslider + fivesmallest
                        if ((total < amount) and (slider_value < 100)):
                            #######################
                            ##  Stage 1          ##
                            ##  shift slider up  ##
                            #######################
                            slider_value += 1
                            self.root.get_screen('Receive2').ids.slider.value = slider_value
                        if ((total < amount) and (slider_value == 100)):
                            ###############################
                            ##  Stage 2                  ##
                            ##  Increase smallest group  ##
                            ###############################
                            startitem += 1
                            enditem += 1
                    ######################################################
                    ##  at this stage:                                  ##
                    ##  the 5 smallest range from startitem to enditem  ##
                    ######################################################
                    for i in range(0, 5):
                        if (self.snapshot_items > i):
                            self.indexlist[i] = self.sortedlist[startitem + i][5]  # index of snapshot_person
                            self.amountlist[i] = self.sortedlist[startitem + i][2]  # amounts
                #####################
                ##  END ascending  ##
                #####################
                if (mode == "random"):
                    #####################################################
                    ##  Put a random snapshot person in the indexlist  ##
                    ##  and add his available coins in the amountlist  ##
                    #####################################################
                    for i in range(0, 5):
                        self.indexlist[i] = random.randint(0, (self.snapshot_items - 1))
                        self.amountlist[i] = self.snapshot_person[self.indexlist[i]][2]
                        #######################################################################
                        ##  for each 1 that is outside the snapshotlist (if it's < 5 items)  ##
                        ##  remove the person by putting -1 on regarding index slot          ##
                        #######################################################################
                        if (i > self.snapshot_items - 1):
                            self.indexlist[i] = -1
                            self.amountlist[i] = 0
                    fiverandom = self.snapshot_person[self.indexlist[0]][2]
                    if (self.snapshot_items > 1):
                        ############################################
                        ##  replace possible doubles in 2nd slot  ##
                        ############################################
                        while (self.indexlist[1] == self.indexlist[0]):
                            self.indexlist[1] = random.randint(0, (self.snapshot_items - 1))
                            self.amountlist[1] = self.snapshot_person[self.indexlist[1]][2]
                        fiverandom += self.snapshot_person[self.indexlist[1]][2]
                    if (self.snapshot_items > 2):
                        ############################################
                        ##  replace possible doubles in 3rd slot  ##
                        ############################################
                        while ((self.indexlist[2] == self.indexlist[0]) or (self.indexlist[2] == self.indexlist[1])):
                            self.indexlist[2] = random.randint(0, (self.snapshot_items - 1))
                            self.amountlist[2] = self.snapshot_person[self.indexlist[2]][2]
                        fiverandom += self.snapshot_person[self.indexlist[2]][2]
                    if (self.snapshot_items > 3):
                        ############################################
                        ##  replace possible doubles in 4th slot  ##
                        ############################################
                        while ((self.indexlist[3] == self.indexlist[0]) or (self.indexlist[3] == self.indexlist[1]) or
                               (self.indexlist[3] == self.indexlist[2])):
                            self.indexlist[3] = random.randint(0, (self.snapshot_items - 1))
                            self.amountlist[3] = self.snapshot_person[self.indexlist[3]][2]
                        fiverandom += self.snapshot_person[self.indexlist[3]][2]
                    if (self.snapshot_items > 4):
                        ############################################
                        ##  replace possible doubles in 5th slot  ##
                        ############################################
                        while ((self.indexlist[4] == self.indexlist[0]) or (self.indexlist[4] == self.indexlist[1]) or
                               (self.indexlist[4] == self.indexlist[2]) or (self.indexlist[4] == self.indexlist[3])):
                            self.indexlist[4] = random.randint(0, (self.snapshot_items - 1))
                            self.amountlist[4] = self.snapshot_person[self.indexlist[4]][2]
                        fiverandom += self.snapshot_person[self.indexlist[4]][2]
                    ###############################################################
                    ##  calculate the total to see if you can make it fill out   ##
                    ##  we try to keep the slider into place and only shift it   ##
                    ##  when it is sort of clear that no combinations of others  ##
                    ##  can be made to fill to the total amount                  ##
                    ##  If the found random combination is not already enough    ##
                    ##  to fill the entire amount, we will run 1000 random       ##
                    ##  combinations of others to try to fill the amount. If we  ##
                    ##  don't succeed, we will fall back on the Descending       ##
                    ##  combination.                                             ##
                    ###############################################################
                    available_coins = (payer_coins * slider_value / 100) + fiverandom
                    counter = 0
                    while ((available_coins < amount) and (counter < 1000)):
                        for i in range(0, 5):
                            self.indexlist[i] = random.randint(0, (self.snapshot_items - 1))
                            self.amountlist[i] = self.snapshot_person[self.indexlist[i]][2]
                        fiverandom = self.snapshot_person[self.indexlist[0]][2]
                        if (self.snapshot_items > 1):
                            while (self.indexlist[1] == self.indexlist[0]):
                                self.indexlist[1] = random.randint(0, (self.snapshot_items - 1))
                                self.amountlist[1] = self.snapshot_person[self.indexlist[1]][2]
                            fiverandom = fiverandom + self.snapshot_person[self.indexlist[1]][2]
                        if (self.snapshot_items > 2):
                            while ((self.indexlist[2] == self.indexlist[0]) or (
                                    self.indexlist[2] == self.indexlist[1])):
                                self.indexlist[2] = random.randint(0, (self.snapshot_items - 1))
                                self.amountlist[2] = self.snapshot_person[self.indexlist[2]][2]
                            fiverandom = fiverandom + self.snapshot_person[self.indexlist[2]][2]
                        if (self.snapshot_items > 3):
                            while ((self.indexlist[3] == self.indexlist[0]) or (
                                    self.indexlist[3] == self.indexlist[1]) or
                                   (self.indexlist[3] == self.indexlist[2])):
                                self.indexlist[3] = random.randint(0, (self.snapshot_items - 1))
                                self.amountlist[3] = self.snapshot_person[self.indexlist[3]][2]
                            fiverandom = fiverandom + self.snapshot_person[self.indexlist[3]][2]
                        if (self.snapshot_items > 4):
                            while ((self.indexlist[4] == self.indexlist[0]) or (
                                    self.indexlist[4] == self.indexlist[1]) or
                                   (self.indexlist[4] == self.indexlist[2]) or (
                                           self.indexlist[4] == self.indexlist[3])):
                                self.indexlist[4] = random.randint(0, (self.snapshot_items - 1))
                                self.amountlist[4] = self.snapshot_person[self.indexlist[4]][2]
                            fiverandom = fiverandom + self.snapshot_person[self.indexlist[4]][2]
                        available_coins = (payer_coins * slider_value / 100) + fiverandom
                        counter += 1
                    if (available_coins < amount):
                        ###################################################
                        ##  no proper random combination found           ##
                        ##  Set item1 tm 5 equal to the 5 largest items  ##
                        ###################################################
                        for i in range(0, 5):
                            self.indexlist[i] = self.snapshot_items - (5 - i)
                            self.amountlist[i] = self.snapshot_person[self.indexlist[i]][2]
                        for i in range(1, 5):
                            if (self.indexlist[i] > self.snapshot_items):
                                self.indexlist[i] = -1
                                self.amountlist[i] = 0
                        fiverandom = fivelargest
                        total = amount - fiverandom
                        if (total > 0):
                            slider_value = payer_coins / total
                        else:
                            slider_value = 100
                ##################
                ##  END Random  ##
                ##################
            ####################################
            ##  END Calculations with others  ##
            ####################################
        ###########################
        ##  Recalculate amounts  ##
        ###########################
        if (mode != "manual"):
            if (self.indexlist[0] > -1):
                ##############################
                ##  There are others found  ##
                ##############################
                self.payerpart = round((amount * (slider_value / 100)), 2)
                ##################################################################################
                ##  correct payerpart if own coin of payer are not sufficient to pay the share  ##
                ##################################################################################
                if (self.payerpart > self.snapshot_payer[2]):
                    self.payerpart = self.snapshot_payer[2]
                ###################################################################################
                ##  Redistribute others based on ratio: ratio = (otherspart / total amountlist)  ##
                ###################################################################################
                otherspart = amount - self.payerpart
                totalothers = 0
                for i in range(0, 5):
                    totalothers += self.amountlist[i]
                ratio = 0
                if (totalothers > 0):
                    ratio = otherspart / totalothers
                else:
                    ratio = 1
                if (ratio > 1):
                    ratio = 1
                total = 0
                for i in range(0, 5):
                    self.otherslist[i] = round((ratio * self.amountlist[i]), 2)
                    total += self.otherslist[i]
                ########################################
                ##  correct possible rounding errors  ##
                ########################################
                if (slider_value < 100):
                    self.payerpart = amount - total
                    if (amount > 0):
                        slider_value = round(self.payerpart * 100 / amount)
                    if (self.payerpart > payer_coins):
                        self.payerpart = payer_coins
                        slider_value = 100
                    total += self.payerpart
                else:
                    if (ratio < 1):
                        ############################
                        ##  select largest other  ##
                        ############################
                        temp = 0
                        for i in range(1, 4):
                            if self.otherslist[i] > self.otherslist[temp]:
                                temp = i
                        total += self.payerpart
                        total -= self.otherslist[temp]
                        self.otherslist[temp] = amount - total
                        total += self.otherslist[temp]
                    else:
                        ########################################################################
                        ##  Slider=100 and ratio = 1, This probably almost never happens and  ##
                        ##  when it happens, the chance og a rounding error is slim           ##
                        ##  to make sure there is no rounding error we use payer to correct   ##
                        ##  this is because 100 in the slider van also be 99.6%               ##
                        ########################################################################
                        self.payerpart = amount - total
                        if amount > 0:
                            slider_value = round(self.payerpart * 100 / amount)
                        if self.payerpart > payer_coins:
                            self.payerpart = payer_coins
                            slider_value = 100
                        total += self.payerpart
                ######################################
                ## Possibly change slider position  ##
                ######################################
                self.root.get_screen('Receive2').ids.slider.value = slider_value
                ######################################
                ##  Flip Descending List of others  ##
                ######################################
                if (mode == "descending"):
                    enditem = 5
                    if (self.snapshot_items < 5):
                        enditem = self.snapshot_items
                    enditem2 = int(enditem / 2)
                    for i in range(0, enditem2):  # 0..4(5)
                        temp = self.otherslist[i]
                        self.otherslist[i] = self.otherslist[enditem - (i + 1)]
                        self.otherslist[enditem - (i + 1)] = temp
                        temps = self.indexlist[i]
                        self.indexlist[i] = self.indexlist[enditem - (i + 1)]
                        self.indexlist[enditem - (i + 1)] = temps
            else:
                #################################
                ##  There are no others found  ##
                #################################
                self.root.get_screen('Receive2').ids.slider.value = 100
                ##################################################################
                ##  self.root.get_screen('Receive2').ids.slider.disabled: True  ##
                ##  disabling the slider doesnt work...                         ##
                ##################################################################
                self.payerpart = amount
                total = amount
                if (self.payerpart > payer_coins):
                    self.payerpart = payer_coins
                    total = self.payerpart
        else:
            ##################################################
            ## In manual mode we need to reconstruct total  ##
            ##################################################
            total = self.payerpart
            for i in range(0, 5):
                if (i < self.snapshot_items):
                    total += self.otherslist[i]
        ################################################################
        ##  Show names and amounts                                    ##
        ##    self.snapshot_payer[0]       => payer name              ##
        ##    self.snapshot_payer[3]       => payer trans             ##
        ##    self.snapshot_payer[4]       => payer mutual            ##
        ##    payerpart                    => payer amount            ##
        ##  --------------------------------------------------------  ##
        ##    sortedlist[indexlist[0]][0]  => others1 name            ##
        ##    sortedlist[indexlist[0]][3]  => others1 trans           ##
        ##    sortedlist[indexlist[0]][4]  => others1 mutual          ##
        ##    otherslist[0]                => others1 amount          ##
        ##  --------------------------------------------------------  ##
        ##    sortedlist[indexlist[1]][0]  => others2 name            ##
        ##    sortedlist[indexlist[1]][3]  => others2 trans           ##
        ##    sortedlist[indexlist[1]][4]  => others2 mutual          ##
        ##    otherslist[1]                => others2 amount          ##
        ##  --------------------------------------------------------  ##
        ##    sortedlist[indexlist[2]][0]  => others3 name            ##
        ##    sortedlist[indexlist[2]][3]  => others3 trans           ##
        ##    sortedlist[indexlist[2]][4]  => others3 mutual          ##
        ##    otherslist[2]                => others3 amount          ##
        ##  --------------------------------------------------------  ##
        ##    sortedlist[indexlist[3]][0]  => others4 name            ##
        ##    sortedlist[indexlist[3]][3]  => others4 trans           ##
        ##    sortedlist[indexlist[3]][4]  => others4 mutual          ##
        ##    otherslist[3]                => others4 amount          ##
        ##  --------------------------------------------------------  ##
        ##    sortedlist[indexlist[4]][0]  => others5 name            ##
        ##    sortedlist[indexlist[4]][3]  => others5 trans           ##
        ##    sortedlist[indexlist[4]][4]  => others5 mutual          ##
        ##    otherslist[4]                => others5 amount          ##
        ##  --------------------------------------------------------  ##
        ##    self.snapshot_items = number of others in receive_list  ##
        ##        (maximum of 5 in receive_list)                      ##
        ################################################################
        security_code = str(self.snapshot_payer[3])
        if (self.snapshot_payer[4]):
            security_code += "9"
        self.root.get_screen("Receive2").ids.label10.text = security_code  # payer security index
        self.root.get_screen("Receive2").ids.label11.text = self.snapshot_payer[0]  # payer name
        self.root.get_screen("Receive2").ids.label12.text = "{:.2f}".format(self.payerpart) + " U"  # payer amount
        ################################################################
        if (self.snapshot_items > 0):
            security_code = str(self.snapshot_person[self.indexlist[0]][3])
            if (self.snapshot_person[self.indexlist[0]][4]):
                security_code += "9"
            self.root.get_screen("Receive2").ids.label30.text = security_code  # others1 security index
            self.root.get_screen("Receive2").ids.label31.text = self.snapshot_person[self.indexlist[0]][
                0]  # others1 name
            self.root.get_screen("Receive2").ids.label32.text = "{:.2f}".format(
                self.otherslist[0]) + " U"  # others1 coins
        else:
            self.root.get_screen("Receive2").ids.label30.text = ""  # others1 security index
            self.root.get_screen("Receive2").ids.label31.text = ""  # others1 name
            self.root.get_screen("Receive2").ids.label32.text = ""  # others1 coins
        ################################################################
        if (self.snapshot_items > 1):
            security_code = str(self.snapshot_person[self.indexlist[1]][3])
            if (self.snapshot_person[self.indexlist[1]][4]):
                security_code += "9"
            self.root.get_screen("Receive2").ids.label40.text = security_code  # others2 security index
            self.root.get_screen("Receive2").ids.label41.text = self.snapshot_person[self.indexlist[1]][
                0]  # others2 name
            self.root.get_screen("Receive2").ids.label42.text = "{:.2f}".format(
                self.otherslist[1]) + " U"  # others2 coins
        else:
            self.root.get_screen("Receive2").ids.label40.text = ""  # others2 security index
            self.root.get_screen("Receive2").ids.label41.text = ""  # others2 name
            self.root.get_screen("Receive2").ids.label42.text = ""  # others2 coins
        ################################################################
        if (self.snapshot_items > 2):
            security_code = str(self.snapshot_person[self.indexlist[2]][3])
            if (self.snapshot_person[self.indexlist[2]][4]):
                security_code += "9"
            self.root.get_screen("Receive2").ids.label50.text = security_code  # others3 security index
            self.root.get_screen("Receive2").ids.label51.text = self.snapshot_person[self.indexlist[2]][
                0]  # others3 name
            self.root.get_screen("Receive2").ids.label52.text = "{:.2f}".format(
                self.otherslist[2]) + " U"  # others3 coins
        else:
            self.root.get_screen("Receive2").ids.label50.text = ""  # others3 security index
            self.root.get_screen("Receive2").ids.label51.text = ""  # others3 name
            self.root.get_screen("Receive2").ids.label52.text = ""  # others3 coins
        ################################################################
        if (self.snapshot_items > 3):
            security_code = str(self.snapshot_person[self.indexlist[3]][3])
            if (self.snapshot_person[self.indexlist[3]][4]):
                security_code += "9"
            self.root.get_screen("Receive2").ids.label60.text = security_code  # others4 security index
            self.root.get_screen("Receive2").ids.label61.text = self.snapshot_person[self.indexlist[3]][
                0]  # others4 name
            self.root.get_screen("Receive2").ids.label62.text = "{:.2f}".format(
                self.otherslist[3]) + " U"  # others4 coins
        else:
            self.root.get_screen("Receive2").ids.label60.text = ""  # others4 security index
            self.root.get_screen("Receive2").ids.label61.text = ""  # others4 name
            self.root.get_screen("Receive2").ids.label62.text = ""  # others4 coins
        ################################################################
        if (self.snapshot_items > 4):
            security_code = str(self.snapshot_person[self.indexlist[4]][3])
            if (self.snapshot_person[self.indexlist[4]][4]):
                security_code += "9"
            self.root.get_screen("Receive2").ids.label70.text = security_code  # others5 security index
            self.root.get_screen("Receive2").ids.label71.text = self.snapshot_person[self.indexlist[4]][
                0]  # others5 name
            self.root.get_screen("Receive2").ids.label72.text = "{:.2f}".format(
                self.otherslist[4]) + " U"  # others5 coins
        else:
            self.root.get_screen("Receive2").ids.label70.text = ""  # others5 security index
            self.root.get_screen("Receive2").ids.label71.text = ""  # others5 name
            self.root.get_screen("Receive2").ids.label72.text = ""  # others5 coins
        ################################################################
        self.root.get_screen("Receive2").ids.label92.text = "{:.2f}".format(total) + " U"  # total

    #########################
    ##  END Set_Receive()  ##
    #########################

    def go_receive_r(self):
        self.root.current = "Receive"
        self.root.transition.direction = "right"

    def go_receive2(self):
        self.set_receive()
        self.root.current = "Receive2"
        self.root.transition.direction = "left"

    def go_receive2_r(self):
        self.root.current = "Receive2"
        self.root.transition.direction = "right"

    def set_receive_slider(self, instance, value):
        self.set_receive()

    def sort_desc(self):
        self.root.get_screen('Receive2').ids.amount.icon_right = "sort-variant"
        self.set_receive()

    def sort_asc(self):
        self.root.get_screen('Receive2').ids.amount.icon_right = "sort-reverse-variant"
        self.set_receive()

    def sort_rand(self):
        self.root.get_screen('Receive2').ids.amount.icon_right = "shuffle-variant"
        self.set_receive()

    def go_receive3_r(self):
        self.root.current = "Receive3"
        self.root.transition.direction = "right"

    def data_receive4(self):
        data = self.root.get_screen("Receive2").ids.amount.text
        try:
            amount = float(data)
        except ValueError:
            amount = 0
        str2 = self.root.get_screen("Receive2").ids.label12.text[0:-2]
        amount = round(amount, 2) - float(self.root.get_screen("Receive2").ids.label12.text[0:-2])
        if (amount < 0):
            amount = 0
        #####################################################################
        ##  change the sort mode in the receive2 page before you leave it  ##
        #####################################################################
        self.root.get_screen('Receive2').ids.amount.icon_right = "fountain-pen-tip"
        ####################################################
        ##  Fill (max 5) Receive list buttons with items  ##
        ####################################################
        total = 0
        if (self.snapshot_items > 0):
            security_code = str(self.snapshot_person[self.indexlist[0]][3])
            if (self.snapshot_person[self.indexlist[0]][4]):
                security_code += "9"
            self.root.get_screen('Receive4').ids.originator1.text = (
                    "(" + security_code + ") " + self.snapshot_person[self.indexlist[0]][0] + ", " + "{:.2f}".format(
                self.otherslist[0]) + " U")
            self.root.get_screen('Receive4').ids.originator1.opacity = 1
            self.root.get_screen('Receive4').ids.originator1.disabled = False
            self.root.get_screen('Receive4').ids.originator1.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator1.width = self.root.width * 0.9
            total += round(self.otherslist[0], 2)
        else:
            self.root.get_screen('Receive4').ids.originator1.text = ""
            self.root.get_screen('Receive4').ids.originator1.opacity = 0
            self.root.get_screen('Receive4').ids.originator1.disabled = True
            self.root.get_screen('Receive4').ids.originator1.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator1.width = self.root.width * 0.9
        if (self.snapshot_items > 1):
            security_code = str(self.snapshot_person[self.indexlist[1]][3])
            if (self.snapshot_person[self.indexlist[1]][4]):
                security_code += "9"
            self.root.get_screen('Receive4').ids.originator2.text = (
                    "(" + security_code + ") " + self.snapshot_person[self.indexlist[1]][0] + ", " + "{:.2f}".format(
                self.otherslist[1]) + " U")
            self.root.get_screen('Receive4').ids.originator2.opacity = 1
            self.root.get_screen('Receive4').ids.originator2.disabled = False
            self.root.get_screen('Receive4').ids.originator2.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator2.width = self.root.width * 0.9
            total += round(self.otherslist[1], 2)
        else:
            self.root.get_screen('Receive4').ids.originator2.text = ""
            self.root.get_screen('Receive4').ids.originator2.opacity = 0
            self.root.get_screen('Receive4').ids.originator2.disabled = True
            self.root.get_screen('Receive4').ids.originator2.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator2.width = self.root.width * 0.9
        if (self.snapshot_items > 2):
            security_code = str(self.snapshot_person[self.indexlist[2]][3])
            if (self.snapshot_person[self.indexlist[2]][4]):
                security_code += "9"   # was "M", change todo
            self.root.get_screen('Receive4').ids.originator3.text = (
                    "(" + security_code + ") " + self.snapshot_person[self.indexlist[2]][0] + ", " + "{:.2f}".format(
                self.otherslist[2]) + " U")
            self.root.get_screen('Receive4').ids.originator3.opacity = 1
            self.root.get_screen('Receive4').ids.originator3.disabled = False
            self.root.get_screen('Receive4').ids.originator3.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator3.width = self.root.width * 0.9
            total += round(self.otherslist[2], 2)
        else:
            self.root.get_screen('Receive4').ids.originator3.text = ""
            self.root.get_screen('Receive4').ids.originator3.opacity = 0
            self.root.get_screen('Receive4').ids.originator3.disabled = True
            self.root.get_screen('Receive4').ids.originator3.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator3.width = self.root.width * 0.9
        if (self.snapshot_items > 3):
            security_code = str(self.snapshot_person[self.indexlist[3]][3])
            if (self.snapshot_person[self.indexlist[3]][4]):
                security_code += "9"   # was "M", change todo
            self.root.get_screen('Receive4').ids.originator4.text = (
                    "(" + security_code + ") " + self.snapshot_person[self.indexlist[3]][0] + ", " + "{:.2f}".format(
                self.otherslist[3]) + " U")
            self.root.get_screen('Receive4').ids.originator4.opacity = 1
            self.root.get_screen('Receive4').ids.originator4.disabled = False
            self.root.get_screen('Receive4').ids.originator4.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator4.width = self.root.width * 0.9
            total += round(self.otherslist[3], 2)
        else:
            self.root.get_screen('Receive4').ids.originator4.text = ""
            self.root.get_screen('Receive4').ids.originator4.opacity = 0
            self.root.get_screen('Receive4').ids.originator4.disabled = True
            self.root.get_screen('Receive4').ids.originator4.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator4.width = self.root.width * 0.9
        if (self.snapshot_items > 4):
            security_code = str(self.snapshot_person[self.indexlist[4]][3])
            if (self.snapshot_person[self.indexlist[4]][4]):
                security_code += "9"   # was "M", change todo
            self.root.get_screen('Receive4').ids.originator5.text = (
                    "(" + security_code + ") " + self.snapshot_person[self.indexlist[4]][0] + ", " + "{:.2f}".format(
                self.otherslist[4]) + " U")
            self.root.get_screen('Receive4').ids.originator5.opacity = 1
            self.root.get_screen('Receive4').ids.originator5.disabled = False
            self.root.get_screen('Receive4').ids.originator5.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator5.width = self.root.width * 0.9
            total += round(self.otherslist[4], 2)
        else:
            self.root.get_screen('Receive4').ids.originator5.text = ""
            self.root.get_screen('Receive4').ids.originator5.opacity = 0
            self.root.get_screen('Receive4').ids.originator5.disabled = True
            self.root.get_screen('Receive4').ids.originator5.size_hint = (0.8, 0.05)
            self.root.get_screen('Receive4').ids.originator5.width = self.root.width * 0.9
        self.root.get_screen('Receive4').ids.status.size_hint = (0.8, 0.05)
        self.root.get_screen('Receive4').ids.status.text = "{:.2f}".format(total) + " [" + "{:.2f}".format(amount) + "]"
        self.root.get_screen('Receive4').ids.status.pos_hint = {'center_x': 0.5}
        self.root.get_screen('Receive4').ids.status.theme_text_color = "Custom"
        self.root.get_screen('Receive4').ids.status.text_color = color_list.get(self.color_selected.lower())

    def go_receive4(self):
        ###################################
        ##  Retrieve the payment amount  ##
        ###################################
        data = self.root.get_screen("Receive2").ids.amount.text
        try:
            amount = float(data)
        except ValueError:
            amount = 0
        amount = round(amount, 2) - float(self.root.get_screen("Receive2").ids.label12.text[0:-2])
        if (amount < 0):
            amount = 0
        if ((self.snapshot_items < 1) or (amount == 0)):
            self.root.current = "Receive2"
            self.root.transition.direction = "right"
        else:
            self.other_manual0 = -1
            self.data_receive4()
            self.root.current = "Receive4"
            self.root.transition.direction = "left"

    def go_receive4_r(self):
        ############################################################
        ##  Here newly chosen others from receive5 are processed  ##
        ############################################################
        self.otherslist[self.other_manual0] = float(self.root.get_screen('Receive5').ids.amount.text)
        self.indexlist[self.other_manual0] = self.other_manual
        self.data_receive4()
        ######################################
        ##  Also change Receive2 page here  ##
        ######################################
        self.set_receive()

        ############################################################
        ##  other_manual0 is set to -1 to so when a user goes     ##
        ##  from receive4 to receive5, the origin is reset        ##
        ############################################################
        self.other_manual0 = -1
        self.data_receive4()
        self.root.current = "Receive4"
        self.root.transition.direction = "right"

    def go_receive4_r_cancel(self):
        """
        Here we go back to receive5 without processing

        - other_manual0 is set to -1 to so when a user goes
        - from receive4 to receive5, the origin is reset
        """
        self.other_manual0 = -1
        self.root.current = "Receive4"
        self.root.transition.direction = "right"

    def originators(self, number):
        """
        Create Originator list in screen Receive 5

        - other_manual0 is button number, will be set to the number of the button that is pressed.
        - other_manual0 must be displayed on Receive5
        - other_manual1..4 are the indexes that are not to be allowed in the list on Receive5 page
        - other_manual0 can only be set when coming from receive4 page
        - number can be 0..4, and is marking the 5 buttons on Receive4 page
        """
        data = self.root.get_screen("Receive2").ids.amount.text
        try:
            self.amount_goal = float(data) - self.payerpart
        except ValueError:
            self.amount_goal = 0
        if self.other_manual0 == -1:
            self.other_manual0 = number
            # other_manual1..4 are not displayed in manual list in Receive5 page
            if number == 0:
                self.other_manual1 = self.indexlist[1]
                self.other_manual2 = self.indexlist[2]
                self.other_manual3 = self.indexlist[3]
                self.other_manual4 = self.indexlist[4]
                self.amount_goal = self.amount_goal - (
                            self.otherslist[1] + self.otherslist[2] + self.otherslist[3] + self.otherslist[4])
            if number == 1:
                self.other_manual1 = self.indexlist[0]
                self.other_manual2 = self.indexlist[2]
                self.other_manual3 = self.indexlist[3]
                self.other_manual4 = self.indexlist[4]
                self.amount_goal = self.amount_goal - (
                            self.otherslist[0] + self.otherslist[2] + self.otherslist[3] + self.otherslist[4])
            if number == 2:
                self.other_manual1 = self.indexlist[0]
                self.other_manual2 = self.indexlist[1]
                self.other_manual3 = self.indexlist[3]
                self.other_manual4 = self.indexlist[4]
                self.amount_goal = self.amount_goal - (
                            self.otherslist[0] + self.otherslist[1] + self.otherslist[3] + self.otherslist[4])
            if number == 3:
                self.other_manual1 = self.indexlist[0]
                self.other_manual2 = self.indexlist[1]
                self.other_manual3 = self.indexlist[2]
                self.other_manual4 = self.indexlist[4]
                self.amount_goal = self.amount_goal - (
                            self.otherslist[0] + self.otherslist[1] + self.otherslist[2] + self.otherslist[4])
            if number == 4:
                self.other_manual1 = self.indexlist[0]
                self.other_manual2 = self.indexlist[1]
                self.other_manual3 = self.indexlist[2]
                self.other_manual4 = self.indexlist[3]
                self.amount_goal = self.amount_goal - (
                            self.otherslist[0] + self.otherslist[1] + self.otherslist[2] + self.otherslist[3])
            self.amount_goal = round(self.amount_goal, 2)
            self.root.get_screen('Receive5').ids.receive5title.title = self.snapshot_person[self.indexlist[number]][0]
            self.root.get_screen('Receive5').ids.amount.text = "{:.2f}".format(self.otherslist[number])
        if self.root.get_screen('Receive5').ids.check1.active:  # securitycode
            sortmode = 3
        if self.root.get_screen('Receive5').ids.check2.active:  # name
            sortmode = 0
        if self.root.get_screen('Receive5').ids.check3.active:  # amount
            sortmode = 2
        # create a new sort list based on the radio buttons
        self.sortedlist2 = sorted(self.snapshot_person, key=lambda x: x[sortmode])
        # Clear possible previous list items (widgets)
        self.root.get_screen('Receive5').ids.container.clear_widgets()
        # Add listitems from others list
        if self.snapshot_items > 0:
            customwidget = []
            widgetcounter = -1
            for i in range(0, self.snapshot_items):
                t = self.sortedlist2[i][5]  # is the index of the item in the snapshot_person[] list
                if ((t != self.other_manual1) and (t != self.other_manual2) and (t != self.other_manual3) and (
                        t != self.other_manual4)):
                    security_code = str(self.sortedlist2[i][3])
                    if self.sortedlist2[i][4]:
                        security_code += "9"   # was "M", change todo
                    customwidget.append(CustomWidget())
                    widgetcounter += 1
                    customwidget[widgetcounter].text = "(" + security_code + ") " + self.sortedlist2[i][
                        0] + ", " + "{:.2f}".format(self.sortedlist2[i][2]) + " U"
                    customwidget[widgetcounter].id = str(self.sortedlist2[i][5])
                    customwidget[widgetcounter].theme_text_color = "Custom"
                    customwidget[widgetcounter].text_color = color_list.get(self.color_selected.lower())
                    self.root.get_screen('Receive5').ids.container.add_widget(customwidget[widgetcounter])
            # self.other_manual is the index of the person clicked in the Receive5 list
            self.other_manual = self.indexlist[number]
            self.root.current = "Receive5"
            self.root.transition.direction = "left"

    def set_amount(self):
        """
        Function after entering amount
        in Receive5 screen
        If the amount is more than the
        person 'owns', amount needs to
        be reduced.
        """
        amount = float(self.root.get_screen('Receive5').ids.amount.text)
        max_amount = self.snapshot_person[self.other_manual][2]
        if amount > max_amount:
            self.root.get_screen('Receive5').ids.amount.text = "{:.2f}".format(max_amount)

    def select_originator(self, key: str):
        """
        When somebody clicks an item in the list on Receive5
        the id is returned
        """
        self.other_manual = int(key)
        self.root.get_screen('Receive5').ids.receive5title.title = self.snapshot_person[self.other_manual][
            0]  # name selected person
        amount_selected = self.snapshot_person[self.other_manual][2]  # amount selected person
        amount = amount_selected
        if amount > self.amount_goal:
            amount = self.amount_goal
        self.root.get_screen('Receive5').ids.amount.text = "{:.2f}".format(amount)

    def sort_manual(self, manual_mode: str):
        if manual_mode == 1:
            self.root.get_screen('Receive5').ids.check1.active = True
        if manual_mode == 2:
            self.root.get_screen('Receive5').ids.check2.active = True
        if manual_mode == 3:
            self.root.get_screen('Receive5').ids.check3.active = True
        self.originators(self.other_manual0)

    def send_proposal(self):
        """ check if amount == total """
        data = self.root.get_screen("Receive2").ids.amount.text
        try:
            amount = float(data)
        except ValueError:
            amount = 0
        amount = round(amount, 2)
        total = round(float(self.root.get_screen("Receive2").ids.label92.text[0:-2]), 2)
        if total == amount and total > 0:
            self.root.current = "Receive3"
            self.root.transition.direction = "left"
        else:
            toast(self.i18n('total_is_not_equal_to_amount'), duration=3)

    def file_manager_open(self):
        self.file_manager.show(os.path.expanduser("~"))  # output manager to the screen
        self.manager_open = True

    def select_path(self, path: str):
        """
        It will be called when you click on the file name
        or the catalog selection button.

        :param path: path to the selected directory or file;
        """

        self.exit_manager()
        self.user_photo_path = path
        path = '/home/teun/Documents/KivyMD/IMG-20150422-WA0005.jpg'
        self.my_image = Image(source=path)
        self.img_width = self.my_image.texture.size[0]
        self.img_height = self.my_image.texture.size[1]
        self.pos_x = 180 - int(self.img_width / 2)
        self.pos_y = 300 - int(self.img_height / 2)
        self.root.get_screen('Set_Image').canvas.before.add(RoundedRectangle(
            source=path, id='rect1',
            pos=(self.pos_x, self.pos_y),
            size=(self.img_width, self.img_height)))

    def exit_manager(self, *args):
        """ Called when the user reaches the root of the directory tree."""

        self.manager_open = False
        self.file_manager.close()

    def events(self, instance, keyboard, keycode, text, modifiers):
        """ Called when buttons are pressed on the mobile device."""

        if keyboard in (1001, 27):
            if self.manager_open:
                self.file_manager.back()
        return True

    def image_slider(self, instance, value):
        slider_value = int(self.root.get_screen('Set_Image').ids.slider.value)

        path = '/home/teun/Documents/KivyMD/IMG-20150422-WA0005.jpg'
        self.my_image = Image(source=path)
        self.img_width = self.my_image.texture.size[0]
        self.img_height = self.my_image.texture.size[1]

        radio_1 = self.root.get_screen('Set_Image').ids.check1.active
        radio_2 = self.root.get_screen('Set_Image').ids.check2.active
        radio_3 = self.root.get_screen('Set_Image').ids.check3.active
        radio_4 = self.root.get_screen('Set_Image').ids.check4.active
        radio_5 = self.root.get_screen('Set_Image').ids.check5.active

        if radio_1:  # hor
            self.pos_x = 180 - int(self.img_width / 2)
            self.sl_hor_pos = (slider_value - 50) * 5
            self.pos_x = self.pos_x + self.sl_hor_pos

        if radio_2:  # ver
            self.pos_y = 300 - int(self.img_height / 2)
            self.sl_ver_pos = (slider_value - 50) * 5
            self.pos_y = self.pos_y + self.sl_ver_pos

        if radio_3:  # scale
            self.sl_scale = ((slider_value - 50) * 1.9) + 100

        self.root.get_screen('Set_Image').canvas.before.add(RoundedRectangle(
            pos=(0, 0),
            size=(2000, 2000)))
        self.root.get_screen('Set_Image').canvas.before.add(RoundedRectangle(
            source=path, id='rect1',
            pos=(self.pos_x, self.pos_y),
            size=(int(self.img_width * self.sl_scale / 100), int(self.img_height * self.sl_scale / 100))))

    def rotatephoto(self):
        print("Rotate Photo")

    def savephoto(self):
        self.root.get_screen('Set_Image').canvas.export_to_png('test.png')
        print("Save Photo")

    def showmanual(self):
        core.show_manual()

    def fraud(self):
        toast("TODO!", duration=3)

    def showdisclaimer(self):
        core.show_disclaimer()


if __name__ == "__main__":
    """ Start the application """
    OneCoinHApp().run()

