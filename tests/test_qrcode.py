""" QRCode testing module """
import glob
import os
from pathlib import Path
from onecoinh import decode_qr, Identity, OneCoinH, Transaction
from .helper import clean


class TestQrCode:
    """ QRCode testing class """

    @classmethod
    def setup_class(cls):
        """Set up the class"""
        clean()

    @classmethod
    def teardown_class(cls):
        """ teardown """
        # print("Cleaning files created by the TestQrCode class")
        images = glob.glob('./tests/fixtures/*.png')

        for image in images:
            if Path(image).is_file():
                Path(image).unlink()

    def test_qr_identity_create(self, random_identity):  # pylint: disable=redefined-outer-name
        """ Test the creation of an identity qr code """
        _i = random_identity()
        _file = f'./tests/fixtures/{_i.hash}'
        os.makedirs(os.path.dirname(_file), exist_ok=True)
        img = _i.generate_code()
        img.save(f"{_file}.png")
        # See if file exists
        assert Path(f"{_file}.png").is_file()
        assert _i.hash in f"{_file}.png"

    def test_qr_transaction_create(self, main_identity, random_identity):  # pylint: disable=redefined-outer-name
        """ Test the creation of an identity qr code """
        app = OneCoinH()
        _u2 = random_identity()
        app.set_identity(main_identity)
        assert app.identity is not None
        assert app.birthday_epoch_diff() == -429960
        _transaction = app.send_transaction(
            _u2.hash,
            4000,
            "Give coins to John",
            validated=False
        )
        _file = f'./tests/fixtures/{_transaction.hash}'
        os.makedirs(os.path.dirname(_file), exist_ok=True)
        img = _transaction.generate_code()
        img.save(f"{_file}.png")
        # See if file exists
        assert Path(f"{_file}.png").is_file()
        assert _transaction.hash in f"{_file}.png"
        Path(f"{_file}.png").unlink()

    def test_qr_identity_read(self):
        """
        Read the images in the fixture directory and see
        if they contain valid identities
        """
        images = glob.glob('./tests/fixtures/*.png')

        for image in images:
            if Path(image).is_file():
                _test = decode_qr(image)
                # test if the hash is in the file name
                if isinstance(_test, Identity):
                    assert _test.hash in image
                if isinstance(_test, Transaction):
                    assert _test.hash in image
