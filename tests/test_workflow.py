""" Workflow testing module """
from random import choice

import pytest
from faker import Faker

from onecoinh import OneCoinH
from .helper import clean


def rand_send_receive(other_hash: str, app: OneCoinH):
    """ Send or receive based on flip coin """
    fake = Faker()
    coin_flip = fake.pybool()
    _text = fake.text(max_nb_chars=48)
    _amount = fake.random_int(min=10, max=420)
    if coin_flip:
        # send us some
        app.send_transaction(other_hash, _amount, _text)
    else:
        # receive some
        app.receive_transaction(other_hash, _amount, _text)


class TestWorkFlow:
    """ Class for testing OneCoinH logic """

    @classmethod
    def setup_class(cls):
        """Set up the class"""
        clean()

    @classmethod
    def teardown_class(cls):
        """teardown any state that was previously setup with a call to
        setup_class.
        """
        clean()

    def test_workflow(self, main_identity, john_doe):  # pylint: disable=redefined-outer-name
        """
        Test a full workflow where we start the app,
        create an identity and do some transactions with the identity
        """
        # print("John Doe", john_doe.hash)
        # Open the Application
        app = OneCoinH()
        # print("Jane Doe", main_identity.hash)
        assert app.identity.is_complete() is False
        assert app.epoch.strftime("%Y-%m-%dT%H:%M:%S") == '2022-01-01T00:00:00'
        # Create identity
        app.set_identity(main_identity)
        assert app.identity is not None
        assert app.birthday_epoch_diff() == -429960
        # print(app.identity.describe())
        # Is it the first Identity?
        assert app.has_previous_identities() is False
        # Is the balance larger than the amount of hours between 2020-01-01 and 2024-2-1 16:00?
        _opening_balance = app.balance()
        assert _opening_balance > 12280
        app.send_transaction(john_doe.hash, 4000, "Give coins to John", validated=True)
        assert app.balance() == _opening_balance - 4000
        app.receive_transaction(john_doe.hash, 4000, "Receive coins from John", validated=True)
        assert app.balance() == _opening_balance
        assert app.blockchain.chain_length == 3

        # Send and receive and only validate after both are set
        app.send_transaction(john_doe.hash, 100, "Pay John for work")  # -100
        app.receive_transaction(john_doe.hash, 400, "Receive from John for product")  # +400
        app.validate_transactions()
        assert app.blockchain.chain_length == 4
        assert app.balance() == _opening_balance + 300
        app.print_transactions()

    def test_reset_identity(self, main_identity):  # pylint: disable=redefined-outer-name
        """
        Try to change existing identity,
        this should fail and the blockchain
        should remain the same length
        """
        app = OneCoinH()
        assert app.identity is not None

        with pytest.raises(ValueError) as e:
            # Create identity, should raise error
            app.set_identity(main_identity)
            assert e == "Identity has already been created"

        assert app.blockchain.chain_length == 4
        app.print_transactions()

    def test_add_another_transaction_identity(self, john_doe):  # pylint: disable=redefined-outer-name
        """ Do some more transactions with John """
        app = OneCoinH()
        assert app.identity is not None
        assert app.blockchain.chain_length == 4
        balance = app.balance()
        app.send_transaction(
            john_doe.hash,
            2,
            "Thank you John for selling me fruit. Thanks, Jane")  # -2
        app.receive_transaction(
            john_doe.hash,
            43,
            "My share for last nights dinner. Cheers, John")  # +43
        app.validate_transactions()
        assert app.blockchain.chain_length == 5
        app.print_transactions()
        assert app.balance() == balance + 41

    def test_loop(self, random_identity):
        """
        Test if we can create a bunch of transactions
        with 4 random identities and generate a balance sheet
        that shows for all users how much coins they sent us
        and how much coins they received from us
        """
        app = OneCoinH()
        assert app.blockchain.chain_length == 5
        identities = []
        assert app.identity is not None
        for _ in range(4):
            identities.append(random_identity())

        for _ in range(100):
            _u = choice(identities)
            rand_send_receive(_u.hash, app)
        # show sheet
        app.validate_transactions()
        # All transactions will be in a single block
        assert app.blockchain.chain_length == 6
        app.print_transactions()
        app.print_transactions_per_user()
        for _ori in app.blockchain.chain[3].data:
            print(_ori.__dict__)
        # print(app.blockchain.chain[3].__dict__)
        # print(app.blockchain.chain[5].__dict__)
