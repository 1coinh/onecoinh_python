""" Identity testing module """
import os
from datetime import date

from onecoinh import Identity, decode_identity_data


class TestIdentity:
    """ Identity testing class """
    def test_identity(self):
        """ Test if we can create an Identity """
        identity = Identity(
            "Mike White",
            date(1972, 8, 4),
            1.90,
            98,
            "brown",
            "brown",
            "male"
        )
        # print(identity.describe())
        # print("Mike White", identity.hash)
        assert identity.eyes == ("brown", "brown")
        assert identity.name == "Mike White"
        assert identity.age() == 51

    def test_one(self, random_identity):
        """ Test if we can create a random identity """
        # print("hash", _one.hash)
        # print("desc", _one.describe())
        _identity = random_identity()
        assert len(_identity.hash) == 64

    def test_loop(self, random_identity):
        """
        Test if we can create 3 random identities,
        save them and read the configuration from the QR image
        """
        for _ in range(3):
            _u = random_identity()
            _file = f'./tests/fixtures/{_u.hash}'
            os.makedirs(os.path.dirname(_file), exist_ok=True)
            img = _u.generate_code()
            img.save(f"{_file}.png")
            _new_u: Identity = decode_identity_data(_u._encode())  # pylint: disable=protected-access
            assert _u.hash == _new_u.hash
            assert _u.describe() == _new_u.describe()
