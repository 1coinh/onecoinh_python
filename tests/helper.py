""" Test helper module """
import glob
import pickle
from datetime import date
from pathlib import Path
from typing import List

from onecoinh import Identity


def clean():
    """ Clean up the data directory """
    _files = glob.glob('data/identity')
    _files.extend(glob.glob('data/*.chain'))
    _files.extend(glob.glob('data/.*.parts'))
    _files.extend(glob.glob('tests/fixtures/*'))
    for _f in _files:
        if Path(_f).is_file():
            Path(_f).unlink()


def fifty_identities() -> List[Identity]:
    """ A list of fifty semi-random identities for use in simulations """
    example_personal_blockchain = [
        Identity("Joan Apple", date(1998, 8, 2), 1.67, None, "blond", "blue",
                 "female", None, "image_0001.jpg"),
        Identity("Barry Graystone", date(1958, 4, 12), 1.84, None, "hazel",
                 "brown", "male", None, "image_0002.jpg"),
        Identity("Gina Plutz", date(2002, 12, 22), 1.69, None, "chesnutbrown",
                 "amber", "female", None, "image_0003.jpg"),
        Identity("Peter Winding", date(1978, 4, 12), 1.88, None, "darkblond",
                 "brown", "male", None, "image_0004.jpg"),
        Identity("Amber Hertz", date(1989, 12, 22), 1.62, None, "black",
                 "brown", "female", None, "image_0005.jpg"),
        Identity("Jimmy Calsons", date(1999, 4, 12), 1.78, None, "darkblond",
                 "bluegray", "male", None, "image_0006.jpg"),
        Identity("Friday Ambona", date(2001, 9, 25), 1.57, None, "black",
                 "brown", "female", None, "image_0007.jpg"),
        Identity("Harry Gonzales", date(1983, 7, 8), 1.85, None, "darkblond",
                 "bluegray", "male", None, "image_0008.jpg"),
        Identity("Birdy Holagis", date(2013, 9, 25), 1.47, None, "darkblond",
                 "brown", "female", None, "image_0009.jpg"),
        Identity("Stelton Manutu", date(1995, 5, 5), 1.72, None, "black",
                 "brown", "male", None, "image_0010.jpg"),
        Identity("Frida Carlson", date(1987, 3, 7), 1.78, None, "medblond",
                 "blue", "female", None, "image_0011.jpg"),
        Identity("Frank Stones", date(1998, 2, 1), 1.87, None, "black", "brown",
                 "male", None, "image_0012.jpg"),
        Identity("Betina Metzer", date(1994, 8, 18), 1.81, None, "blond",
                 "amber", "female", None, "image_0013.jpg"),
        Identity("Hans Vertongen", date(1986, 9, 30), 1.81, None, "medblond",
                 "green", "male", None, "image_0014.jpg"),
        Identity("Shane Tampar", date(1994, 2, 12), 1.57, None, "black",
                 "brown", "female", None, "image_0015.jpg"),
        Identity("Ferry Palsting", date(1983, 3, 3), 1.85, None, "darkblond",
                 "gray", "male", None, "image_0016.jpg"),
        Identity("Gwen Waldsora", date(2002, 4, 22), 1.72, None, "black",
                 "brown", "female", None, "image_0017.jpg"),
        Identity("Pete Bantogava", date(1981, 11, 7), 1.77, None, "black",
                 "brown", "male", None, "image_0018.jpg"),
        Identity("Thea Quenta", date(2000, 3, 9), 1.72, None, "black", "brown",
                 "female", None, "image_0019.jpg"),
        Identity("John Frankson", date(1979, 2, 1), 1.81, None, "darkblond",
                 "gray", "male", None, "image_0020.jpg"),
        Identity("Mildred Hetzano", date(1998, 7, 11), 1.72, None, "black",
                 "brown", "female", None, "image_0021.jpg"),
        Identity("Harold Johnson", date(1983, 7, 28), 1.80, None, "darkblond",
                 "blue", "male", None, "image_0022.jpg"),
        Identity("Tuesday Kampala", date(2004, 6, 1), 1.72, None, "black",
                 "brown", "female", None, "image_0023.jpg"),
        Identity("Timo Schaumann", date(1990, 1, 22), 1.84, None, "darkblond",
                 "green", "male", None, "image_0024.jpg"),
        Identity("Mel Ton", date(1998, 11, 30), 1.57, None, "darkblond",
                 "brown", "female", None, "image_0025.jpg"),
        Identity("Ben Vanderslag", date(2002, 12, 7), 1.82, None, "darkblond",
                 "brown", "male", None, "image_0026.jpg"),
        Identity("Carla Manders", date(1981, 8, 11), 1.78, None, "medblond",
                 "bluegreen", "female", None, "image_0027.jpg"),
        Identity("Gino Plessens", date(1991, 12, 18), 1.88, None, "darkblond",
                 "brown", "male", None, "image_0028.jpg"),
        Identity("Mabel Van Hora", date(2002, 5, 8), 1.72, None, "black",
                 "brown", "female", None, "image_0029.jpg"),
        Identity("Hal Bransor", date(1968, 4, 5), 1.83, None, "darkblond",
                 "bluegray", "male", None, "image_0030.jpg"),
        Identity("Patty Gwentana", date(2002, 3, 10), 1.76, None, "hazel",
                 "hazel", "female", None, "image_0031.jpg"),
        Identity("Luther Pontona", date(1993, 2, 22), 1.80, None, "black",
                 "brown", "male", None, "image_0032.jpg"),
        Identity("Mandy Peters", date(2006, 4, 2), 1.66, None, "black", "brown",
                 "female", None, "image_0033.jpg"),
        Identity("Hans Burtpal", date(1980, 4, 9), 1.80, None, "darkblond",
                 "green", "male", None, "image_0034.jpg"),
        Identity("Helga Schimanski", date(1993, 10, 5), 1.82, None, "blond",
                 "blue", "female", None, "image_0035.jpg"),
        Identity("Ton Bekkers", date(2003, 2, 2), 1.78, None, "medblond",
                 "bluegray", "male", None, "image_0036.jpg"),
        Identity("Cassy Ventura", date(1998, 10, 10), 1.72, None, "black",
                 "brown", "female", None, "image_0037.jpg"),
        Identity("Harry Weerter", date(1998, 4, 21), 1.82, None, "blond",
                 "blue", "male", None, "image_0038.jpg"),
        Identity("Margareth Neus", date(2000, 2, 4), 1.66, None, "black",
                 "brown", "female", None, "image_0039.jpg"),
        Identity("Max Teneos", date(1988, 6, 16), 1.85, None, "darkblond",
                 "bluegray", "male", None, "image_0040.jpg"),
        Identity("Sandy Mo", date(1982, 8, 14), 1.71, None, "hazel", "green",
                 "female", None, "image_0041.jpg"),
        Identity("Benny Crups", date(1997, 7, 17), 1.83, None, "hazel", "blue",
                 "male", None, "image_0042.jpg"),
        Identity("Helga Friedman", date(2004, 4, 5), 1.76, None, "medblond",
                 "blue", "female", None, "image_0043.jpg"),
        Identity("Tamsi Kutama", date(1982, 2, 18), 1.74, None, "black",
                 "brown", "male", None, "image_0044.jpg"),
        Identity("Donna Winter", date(1999, 11, 2), 1.74, None, "blond", "blue",
                 "female", None, "image_0045.jpg"),
        Identity("Theo Kasperos", date(1957, 8, 9), 1.78, None, "gray",
                 "bluegray", "male", None, "image_0046.jpg"),
        Identity("Maxime Thendo", date(1999, 4, 12), 1.75, None, "black",
                 "brown", "female", None, "image_0047.jpg"),
        Identity("Barry Gibson", date(2003, 3, 23), 1.78, None, "medblond",
                 "blue", "male", None, "image_0048.jpg"),
        Identity("Campari Ponuka", date(2000, 3, 10), 1.76, None, "black",
                 "brown", "female", None, "image_0049.jpg"),
        Identity("Peter Quintar", date(2017, 5, 16), 1.22, None, "darkblond",
                 "bluegray", "male", None, "image_0050.jpg")]

    # save as pb.pickle in the data directory
    with open("data/example_pb.pickle", 'wb') as f:
        pickle.dump(example_personal_blockchain, f)
    return example_personal_blockchain

