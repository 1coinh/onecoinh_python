""" Constant testing module """
import onecoinh.constants


class TestConstants:
    """ Constant testing class """
    def test_list_colors(self):
        """ Test if we can get a list of color names and see if amber is in it """
        _c_l = onecoinh.constants.get_color_list()
        assert "amber" in _c_l

    def test_red(self):
        """ Test if the color Enum allows for all the actions we require """
        _c = onecoinh.constants.Colors.RED
        # print(_c.name)
        # print(_c.hex_color(), _c.kivy_color())
        assert _c.hex_color() == "#e52b50"

    def test_bluegray(self):
        """ Test if the color Enum allows for all the actions we require """
        _c = onecoinh.constants.Colors.BLUEGRAY
        # print(_c.name)
        # print(_c.hex_color(), _c.kivy_color())
        assert _c.hex_color() == "#536895"
