""" From scratch testing module """

from .helper import clean


class TestFromScratch:
    """ Class for testing OneCoinH logic """

    @classmethod
    def setup_class(cls):
        """Set up the class"""
        clean()

    def test_create_new_app(self, random_app):
        """
        Start a OneCoinH app.
        Set up the identity, step by step adding all
        properties one by one as the UI would do.

        App sees Identity is complete and starts PB/Chain.

        Make connection with another user (for this, we have to create the other
        user running a second instance op the OneCoinH app and running the same
        process as for our main user). Then we make the connection (how?)

        Start a transaction

        Validate the transaction on both ends

        Write assert functions that check:
        - The transaction only contains coin of ONE originator given that no
          previous transactions are present
        - The "balance" is as we expect it to be.

        """
        _user_one_app = random_app
        _user_two_app = random_app
