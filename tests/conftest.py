""" Testing configuration module """
import random
from datetime import date
from io import BytesIO
from random import uniform

import pytest
from PIL import Image, ImageDraw
from faker import Faker
from faker.providers import DynamicProvider

from onecoinh import Identity, OneCoinH
from onecoinh.constants import get_color_list


@pytest.fixture(name="random_image")
def fixture_random_image():
    """ Generate a random 255 x 255 image """
    fake = Faker()
    _img_bytes = BytesIO()
    # set image
    width = height = 255
    dummy_img = Image.new(mode='RGB', size=(width, height), color=fake.color())

    draw = ImageDraw.Draw(dummy_img)

    # Generate and test random pixels
    for _ in range(1000):
        x = random.randint(0, width - 1)
        y = random.randint(0, height - 1)
        color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        draw.point((x, y), fill=color)
    dummy_img.save(_img_bytes, format='JPEG')
    return _img_bytes.getvalue()


@pytest.fixture
def main_identity(random_image):
    """ Create the main (Jane Doe) identity fixture """
    yield Identity(
        "Jane Doe",
        date(1972, 12, 14),
        1.64,
        57,
        "brown",
        ("blue", "blue"),
        "female",
        None,
        random_image
    )


@pytest.fixture
def john_doe(random_image):
    """ Create the John Doe identity fixture """
    yield Identity(
        "John Doe",
        date(1980, 1, 1),
        1.75,
        65,
        "brown",
        ("blue", None),
        "male",
        None,
        random_image
    )


@pytest.fixture
def random_app(random_image):
    """
    Create a dummy app with the minimal attributes for a valid
    Identity assert them along the way
    """
    fake = Faker()
    _app = OneCoinH()
    # set name
    _app.identity.set_name(fake.name())
    assert _app.identity.is_complete() is False
    # set birthday
    _app.identity.set_dob(fake.passport_dob())
    _app.identity.set_image(random_image)
    assert _app.identity.is_complete() is True
    # (Optional)
    # - set gender
    # - set height
    # - set hair
    # - set left-eye
    # - set right-eye
    # - set special features (optional)

    assert _app.identity is not None
    # Identity has not been saved, blockchain should be None
    assert _app.blockchain is None
    return _app


@pytest.fixture
def random_identity(random_image):
    """ Random Identity Generator Fixture """
    color_provider = DynamicProvider(
        provider_name="onecoinhcolor",
        elements=get_color_list(),
    )
    gender_provider = DynamicProvider(
        provider_name="gender",
        elements=["male", "female", "other"],
    )

    fake = Faker()
    fake.add_provider(gender_provider)
    fake.add_provider(color_provider)

    def generate():
        _image = random_image
        _identity = Identity(
            fake.name(),
            fake.passport_dob(),
            uniform(0.57, 2.1),
            uniform(20, 200),
            fake.onecoinhcolor(),
            (fake.onecoinhcolor(), fake.onecoinhcolor()),
            fake.gender(),
            None,
            _image
        )
        return _identity
    return generate
