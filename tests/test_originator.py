# pylint: disable=fixme
""" Originator testing module """


class TestOriginator:  # pylint: disable=too-few-public-methods
    """ Identity testing class """
    def test_set_identity_on_chain(self, random_identity):
        """ Test if we can create an Identity """
        identity = random_identity()
        print(identity.describe(), identity.hash)
        # todo put the identity on the chain and check while checking if the identity is present
        # todo get the identity index
        # todo create a transaction with the index set as originator
        # todo check the transaction, validating the originator
