""" Simulation """
from datetime import datetime
from io import BytesIO
from pathlib import Path

from PIL import Image

from onecoinh import OneCoinH
from tests.helper import fifty_identities, clean


def calc_own_coins(times: int = 8760):
    """ Calculate coins, default to one year of hours """
    counter = 0
    own_coins = 0
    while counter < times:
        own_coins = (own_coins * 0.99995) + 1
        counter += 1
    return own_coins


def calc_hours(start: datetime, end: datetime):
    """ Calculate the number of hours between two date/times """
    duration = end - start
    return int(divmod(duration.total_seconds(), 3600)[0])


class TestSimulation:
    """ Simulation test class """
    @classmethod
    def setup_class(cls):
        """Set up the class"""
        clean()

    def test_simulation(self):
        """ Run simulation """
        identities = fifty_identities()
        apps = []
        assert identities[3].name == "Peter Winding"
        assert isinstance(identities[3].image, bytes)
        for _id in identities:
            print(_id.name, _id.hash)
            my_image = Image.open(BytesIO(_id.image))
            print(my_image)
        # for i in range(0, 50):
        #     _app = OneCoinH()
        #     _app.identity_pkl = Path(f"tests/fixtures/identity_{i}")
        #     _app.set_identity(identities[i])
        #     print(identities[1].hash)
        #     print(identities[i].name, _app.generated)
        #     apps.append(_app)
